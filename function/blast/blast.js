const knex = require("../../connection.js");
const axios = require('axios')
const moment = require("moment");

async function getUserBlastEmail() {
   let result = null;
   let count = null;
 
   // dateFrom = dateFormat(dateFrom);
   // console.log(`dateFrom: `, dateFrom);
 
   // dateTo = dateFormat(dateTo);
   // console.log(`dateTo: `, dateTo);
 
   result = await knex.connect.raw(`
   SELECT userFullname, userType, userEmail, userPhoneNo
   FROM user
   WHERE userType != 'Admin'

   UNION

   SELECT implementerFullname, 0 as userType, implementerEmail, implementerPhoneNo
   FROM implementer
   ORDER BY userType ASC`)
   // .where(`userStatusCode`, userStatusCode)
   // count = await knex.connect(`user`).count()
   // .where(`userStatusCode`, userStatusCode)
 
//    for (let i = 0; i < result.length; i++) {
//      result[i].userRegistrationDate = moment(result[i].userRegistrationDate).format("YYYY-MM-DD HH:mm:ss")
//  }
 
   // console.log(count[0]["count(*)"]);
 
   return [result];
 }

 async function getEjenBlastEmail() {
   let result = null;
   let count = null;
 
   // dateFrom = dateFormat(dateFrom);
   // console.log(`dateFrom: `, dateFrom);
 
   // dateTo = dateFormat(dateTo);
   // console.log(`dateTo: `, dateTo);
 
   result = await knex.connect.raw(`
   SELECT userFullname, userType, userEmail, userPhoneNo
   FROM user
   WHERE userType = 'Ejen'`)
   // .where(`userStatusCode`, userStatusCode)
   // count = await knex.connect(`user`).count()
   // .where(`userStatusCode`, userStatusCode)
 
//    for (let i = 0; i < result.length; i++) {
//      result[i].userRegistrationDate = moment(result[i].userRegistrationDate).format("YYYY-MM-DD HH:mm:ss")
//  }
 
   // console.log(count[0]["count(*)"]);
 
   return [result];
 }

 async function getWakilBlastEmail() {
   let result = null;
   let count = null;
 
   // dateFrom = dateFormat(dateFrom);
   // console.log(`dateFrom: `, dateFrom);
 
   // dateTo = dateFormat(dateTo);
   // console.log(`dateTo: `, dateTo);
 
   result = await knex.connect.raw(`
   SELECT userFullname, userType, userEmail, userPhoneNo
   FROM user
   WHERE userType = 'Wakil'`)
   // .where(`userStatusCode`, userStatusCode)
   // count = await knex.connect(`user`).count()
   // .where(`userStatusCode`, userStatusCode)
 
//    for (let i = 0; i < result.length; i++) {
//      result[i].userRegistrationDate = moment(result[i].userRegistrationDate).format("YYYY-MM-DD HH:mm:ss")
//  }
 
   // console.log(count[0]["count(*)"]);
 
   return [result];
 }

 async function getImplementerBlastEmail() {
   let result = null;
   let count = null;
 
   // dateFrom = dateFormat(dateFrom);
   // console.log(`dateFrom: `, dateFrom);
 
   // dateTo = dateFormat(dateTo);
   // console.log(`dateTo: `, dateTo);
 
   result = await knex.connect.raw(`
   SELECT implementerFullname AS userFullname, 0 as userType, implementerEmail AS userEmail, implementerPhoneNo AS userPhoneNo
   FROM implementer`)
   // .where(`userStatusCode`, userStatusCode)
   // count = await knex.connect(`user`).count()
   // .where(`userStatusCode`, userStatusCode)
 
//    for (let i = 0; i < result.length; i++) {
//      result[i].userRegistrationDate = moment(result[i].userRegistrationDate).format("YYYY-MM-DD HH:mm:ss")
//  }
 
   // console.log(count[0]["count(*)"]);
 
   return [result];
 }

 async function getListBlast() {
   let result = null;
   let count = null;
 
   // dateFrom = dateFormat(dateFrom);
   // console.log(`dateFrom: `, dateFrom);
 
   // dateTo = dateFormat(dateTo);
   // console.log(`dateTo: `, dateTo);
 
   result = await knex.connect.raw(`
   SELECT *, notificationSentDate AS masa 
   FROM notification
   WHERE notificationRefTable = 'blastEmailSms'
   ORDER BY notificationSentDate DESC`)
   // .where(`userStatusCode`, userStatusCode)
   // count = await knex.connect(`user`).count()
   // .where(`userStatusCode`, userStatusCode)
 
//    for (let i = 0; i < result.length; i++) {
//      result[i].userRegistrationDate = moment(result[i].userRegistrationDate).format("YYYY-MM-DD HH:mm:ss")
//  }
 
   // console.log(count[0]["count(*)"]);
 
   return [result];
 }

 async function blastSmsAPerson(
   phoneNo,
   SMS,
 ) {
   let result = null
   let resultSMS = null
   let userNumber = '+6' + phoneNo
   let userMessage = null
 
   userMessage = SMS
 
   // HANTAR NOTIFIKASI MELALUI SMS
   await axios
     .post(
       'https://manage.smsniaga.com/api/send',
       {
         body: userMessage,
         phones: [userNumber],
         sender_id: 'YAYASAN IKHLAS',
       },
       {
         headers: {
           Authorization: `Bearer ` + process.env.TOKEN,
         },
       },
     )
     .then(function (response) {
       resultSMS = response.statusText
     })
     .catch(function (error) {
       console.log(error)
     })
 
   
 
   result = {
     resultSMS: resultSMS,
   }
 
   return result
 }

 async function blastSmsSeveralPerson(
   phoneNo,
   SMS,
 ) {
   let result = null
   let resultSMS = null
   let userNumber = '+6' + phoneNo
   let userMessage = null
 
   userMessage = SMS
 
   // HANTAR NOTIFIKASI MELALUI SMS
   await axios
     .post(
       'https://manage.smsniaga.com/api/send',
       {
         body: userMessage,
         phones: [userNumber],
         sender_id: 'YAYASAN IKHLAS',
       },
       {
         headers: {
           Authorization: `Bearer ` + process.env.TOKEN,
         },
       },
     )
     .then(function (response) {
       resultSMS = response.statusText
     })
     .catch(function (error) {
       console.log(error)
     })
 
   
 
   result = {
     resultSMS: resultSMS,
   }
 
   return result
 }

 async function insertNotificationBlastSMS(
   organization,
   receiver,
   content,
   type,
   status
 ) {
   let result = null
   let sql = null
   let date = moment().format("YYYY-MM-DD HH:mm:ss"), 
   RefTable = 'blastEmailSms'
   try {
     sql = await knex.connect(`notification`).insert({
       organizationId: organization,
       notificationReceiver: receiver,
       notificationContent: content,
       notificationType: type,
       notificationRefTable: RefTable,
       notificationSentDate: date,
       notificationStatus: status
     })
 
     console.log('insertNotificationBlastSMS: ', sql)
 
     if (!sql || sql.length == 0) {
       result = false
     } else {
       result = sql[0]
     }
   } catch (error) {
     console.log(error)
   }
 
   return result
 }

 async function insertNotificationBlastEmail(
   organization,
   receiver,
   subject,
   content,
   type,
   status
 ) {
   let result = null
   let sql = null
   let date = moment().format("YYYY-MM-DD HH:mm:ss"), 
   RefTable = 'blastEmailSms'
   try {
     sql = await knex.connect(`notification`).insert({
       organizationId: organization,
       notificationReceiver: receiver,
       notificationContent: content,
       notificationSubject: subject,
       notificationType: type,
       notificationRefTable: RefTable,
       notificationSentDate: date,
       notificationStatus: status
     })
 
     console.log('insertNotificationBlastSMS: ', sql)
 
     if (!sql || sql.length == 0) {
       result = false
     } else {
       result = sql[0]
     }
   } catch (error) {
     console.log(error)
   }
 
   return [result,status]
 }

module.exports = {
   getUserBlastEmail,
   getEjenBlastEmail,
   getWakilBlastEmail,
   getImplementerBlastEmail,
   getListBlast,
   blastSmsAPerson,
   blastSmsSeveralPerson,
   insertNotificationBlastSMS,
   insertNotificationBlastEmail,

};