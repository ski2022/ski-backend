const knex = require("../../connection.js");
const moment = require("moment");

async function getHaiwanKorban(year) {
  let jumlahHaiwanKorban = null;
  let jumlahBaki = null;
  // let jumlahBahagian = null;
  // let jumlahBahagianBaki = null;

  jumlahHaiwanKorban = await knex
    .connect(`liveStok`)
    .select(`liveStokType`)
    .count(`liveStokId`, { as: "jumlahKeseluruhan" })
    .sum(`liveStokBilBahagian`, { as: "jumlahBahagian" })
    .sum(`liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
    .where(`setupKorbanId`, year)
    .groupBy("liveStokType");

  jumlahBaki = await knex
    .connect(`liveStok`)
    .select(`liveStokType`)
    .count(`liveStokId`, { as: "jumlahBaki" })
    .where(`liveStokStatus`, `Active`)
    .orWhere(`liveStokStatus`, `Partial`)
    .orWhere(`liveStokStatus`, `Pending Reserved`)
    .andWhere(`setupKorbanId`, year)
    .groupBy("liveStokType");

  // jumlahBahagian = await knex.connect(`liveStok`)
  // .select(`liveStokType`)
  // .sum(`liveStokBilBahagian`, { as: 'jumlahBaki' })
  // .groupBy('liveStokType')

  // jumlahBahagianBaki = await knex.connect(`liveStok`)
  // .select(`liveStokType`)
  // .sum(`liveStokBilBahagianBaki`, { as: 'jumlahBaki' })
  // .groupBy('liveStokType')

  console.log("TEST 1", jumlahHaiwanKorban);
  console.log("TEST 2", jumlahBaki);

  return [jumlahHaiwanKorban, jumlahBaki];
}

async function getImplementerHaiwanKorban(jenisHaiwan, year) {
  let jumlahHaiwanKorban = null;
  let jumlahBaki = null;

  jumlahHaiwanKorban = await knex
    .connect(`liveStok`)
    .select(`implementer.implementerFullname`, `implementer.implementerNegara`)
    .count(`liveStok.liveStokId`, { as: "jumlahKeseluruhan" })
    .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
    .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
    .join(`implementer`, `liveStok.implementerId`, `implementer.implementerId`)
    .where(`liveStok.liveStokType`, jenisHaiwan)
    .andWhere(`liveStok.setupKorbanId`, year)
    .groupBy(`implementer.implementerId`);

  jumlahBaki = await knex
    .connect(`liveStok`)
    .select(`implementer.implementerFullname`, `implementer.implementerNegara`)
    .count(`liveStok.liveStokId`, { as: "jumlahBaki" })
    .join(`implementer`, `liveStok.implementerId`, `implementer.implementerId`)
    .where(`liveStok.liveStokType`, jenisHaiwan)
    .andWhere(`liveStok.setupKorbanId`, year)
    .whereNot(`liveStok.liveStokStatus`, `Full`)
    // .orWhere(`liveStok.liveStokType`, jenisHaiwan)
    // .where(`liveStok.liveStokStatus`, `Partial`)
    .groupBy(`implementer.implementerId`);

  return [jumlahHaiwanKorban, jumlahBaki];
}

async function getButiranHaiwanKorban(jenisHaiwan, jenisIbadah, year) {
  let jumlahHaiwanKorban = null;
  let jumlahBaki = null;

  if (jenisIbadah == "Semua") {
    jumlahHaiwanKorban = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahKeseluruhan" })
      .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
      .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaEkor`, {
        as: "hargaEkor",
      })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaBhgn`, {
        as: "hargaBahagian",
      })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .join(
        `implementerLokasiHaiwan`,
        `liveStok.implementerLokasiHaiwanId`,
        `implementerLokasiHaiwan.implementerLokasiHaiwanId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .groupBy(`implementer.implementerNegara`);

    jumlahBaki = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahBaki" })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .whereNot(`liveStok.liveStokStatus`, `Full`)
      // .orWhere(`liveStok.liveStokType`, jenisHaiwan)
      // .where(`liveStok.liveStokStatus`, `Partial`)
      .groupBy(`implementer.implementerNegara`);
  } else if (jenisIbadah == "Akiqah") {
    jumlahHaiwanKorban = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahKeseluruhan" })
      .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
      .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaEkor`, {
        as: "hargaEkor",
      })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaBhgn`, {
        as: "hargaBahagian",
      })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .join(
        `implementerLokasiHaiwan`,
        `liveStok.implementerLokasiHaiwanId`,
        `implementerLokasiHaiwan.implementerLokasiHaiwanId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .andWhere(`liveStok.liveStokIbadah`, "Akiqah")
      .groupBy(`implementer.implementerNegara`);

    jumlahBaki = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahBaki" })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .andWhere(`liveStok.liveStokIbadah`, "Akiqah")
      .whereNot(`liveStok.liveStokStatus`, `Full`)
      // .orWhere(`liveStok.liveStokType`, jenisHaiwan)
      // .where(`liveStok.liveStokStatus`, `Partial`)
      .groupBy(`implementer.implementerNegara`);
  } else if (jenisIbadah == "Korban") {
    jumlahHaiwanKorban = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahKeseluruhan" })
      .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
      .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaEkor`, {
        as: "hargaEkor",
      })
      .max(`implementerLokasiHaiwan.implementerLokasiHaiwanHargaBhgn`, {
        as: "hargaBahagian",
      })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .join(
        `implementerLokasiHaiwan`,
        `liveStok.implementerLokasiHaiwanId`,
        `implementerLokasiHaiwan.implementerLokasiHaiwanId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .andWhere(`liveStok.liveStokIbadah`, "Korban")
      .groupBy(`implementer.implementerNegara`);

    jumlahBaki = await knex
      .connect(`liveStok`)
      .select(`implementer.implementerNegara`)
      .count(`liveStok.liveStokId`, { as: "jumlahBaki" })
      .join(
        `implementer`,
        `liveStok.implementerId`,
        `implementer.implementerId`
      )
      .where(`liveStok.liveStokType`, jenisHaiwan)
      .andWhere(`liveStok.setupKorbanId`, year)
      .andWhere(`liveStok.liveStokIbadah`, "Korban")
      .whereNot(`liveStok.liveStokStatus`, `Full`)
      // .orWhere(`liveStok.liveStokType`, jenisHaiwan)
      // .where(`liveStok.liveStokStatus`, `Partial`)
      .groupBy(`implementer.implementerNegara`);
  }

  return [jumlahHaiwanKorban, jumlahBaki];
}

module.exports = {
  getHaiwanKorban,
  getImplementerHaiwanKorban,
  getButiranHaiwanKorban,
};
