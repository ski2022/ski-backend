const knex = require("../../connection.js");
const moment = require("moment");

async function get_senarai_haiwan() {
  var sql = await knex.connect.raw(`SELECT 
                                        liveStokType AS HAIWAN,
                                        liveStokIbadah AS IBADAH,
                                        COUNT(liveStokType) AS JUMLAH_HAIWAN, 
                                        SUM(liveStokBilBahagian) AS JUMLAH_BAHAGIAN, 
                                        SUM(liveStokBilBahagian) - SUM(liveStokBilBahagianBaki) AS JUMLAH_BAHAGIAN_TERJUAL,
                                        SUM(liveStokBilBahagianBaki) AS JUMLAH_BAKI_BAHAGIAN,
                                        liveStokType AS BUTIRAN_HAIWAN
                                        
                                    FROM liveStok
                                    GROUP BY liveStokType, liveStokIbadah`);

  return sql[0];
}

module.exports = { get_senarai_haiwan };
