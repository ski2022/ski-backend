const knex = require("../../connection.js");
const moment = require("moment");
const sha = require("sha256");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

// DASHBOARD ADMIN
async function getSetupKorbanId(year) {
  let korbanId = await knex
    .connect("setupKorban")
    .select("setupKorbanId")
    .where("setupKorbanTahun", year);

  return korbanId;
}

async function getStatChartAdmin(year) {
  let jumlahKutipan = null;
  let jumlahKutipanByDate = null;

  let jumlahJualan = null;
  let jumlahJualanByDate = null;

  let ibadahKorban = null;
  let ibadahKorbanByDate = null;

  let ibadahAkikah = null;
  let ibadahAkikahByDate = null;

  jumlahKutipan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahKutipan" })
    .where(`tempahanStatus`, "Success")
    .andWhere(`setupKorbanId`, `=`, year);

  jumlahKutipanByDate = await knex
    .connect("tempahan")
    .sum("tempahanTotalPrice as jumlahKutipan")
    .where("tempahanStatus", "=", "Success")
    .andWhere("setupKorbanId", "=", year)
    .groupByRaw("DATE(tempahanCreatedDate)")
    .orderByRaw("DATE(tempahanCreatedDate) DESC")
    .limit(10);

  // .connect
  // .raw(`SELECT SUM(tempahanTotalPrice) AS jumlahKutipan
  //   FROM tempahan
  //   WHERE tempahanStatus = 'Success'
  //   AND setupKorbanId = `+ year +`
  //   GROUP BY DATE(tempahanCreatedDate)
  //   ORDER BY DATE(tempahanCreatedDate) DESC
  //   LIMIT 10`);

  // console.log("Jumlah Kutipan: ",jumlahKutipanByDate)

  jumlahJualan = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, "Success")
    .andWhere(`setupKorbanId`, `=`, year);

  jumlahJualanByDate = await knex
    .connect("tempahan")
    .count("tempahanId as jumlahJualan")
    .where("tempahanStatus", "=", "Success")
    .andWhere("setupKorbanId", "=", year)
    .groupByRaw("DATE(tempahanCreatedDate)")
    .orderByRaw("DATE(tempahanCreatedDate) DESC")
    .limit(10);

  // await knex.connect
  //   .raw(`SELECT COUNT(tempahanId) AS jumlahJualan
  //   FROM tempahan
  //   WHERE tempahanStatus = 'Success'
  //   GROUP BY DATE(tempahanCreatedDate)
  //   ORDER BY DATE(tempahanCreatedDate) DESC
  //   LIMIT 10`);

  ibadahKorban = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "ibadahKorban" })
    .where(`tempahanStatus`, "Success")
    .andWhere(`tempahanIbadahType`, "Korban")
    .andWhere(`setupKorbanId`, `=`, year);

  ibadahKorbanByDate = await knex
    .connect("tempahan")
    .count("tempahanId as ibadahKorban")
    .where("tempahanStatus", "=", "Success")
    .andWhere("setupKorbanId", "=", year)
    .andWhere("tempahanIbadahType", "=", "Korban")
    .groupByRaw("DATE(tempahanCreatedDate)")
    .orderByRaw("DATE(tempahanCreatedDate) DESC")
    .limit(10);

  // await knex.connect
  //   .raw(`SELECT COUNT(tempahanId) AS ibadahKorban
  //   FROM tempahan
  //   WHERE tempahanStatus = 'Success'
  //   AND tempahanIbadahType = 'Korban'
  //   GROUP BY DATE(tempahanCreatedDate)
  //   ORDER BY DATE(tempahanCreatedDate) DESC
  //   LIMIT 10`);

  ibadahAkikah = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "ibadahAkikah" })
    .where(`tempahanStatus`, "Success")
    .andWhere(`tempahanIbadahType`, "Akikah")
    .andWhere(`setupKorbanId`, `=`, year);

  ibadahAkikahByDate = await knex
    .connect("tempahan")
    .count("tempahanId as ibadahAkikah")
    .where("tempahanStatus", "=", "Success")
    .andWhere("setupKorbanId", "=", year)
    .andWhere("tempahanIbadahType", "=", "Akikah")
    .groupByRaw("DATE(tempahanCreatedDate)")
    .orderByRaw("DATE(tempahanCreatedDate) DESC")
    .limit(10);

  // await knex.connect.raw(`SELECT COUNT(tempahanId) AS ibadahAkikah
  //       FROM tempahan
  //       WHERE tempahanStatus = 'Success'
  //       AND tempahanIbadahType = 'Akiqah'
  //       GROUP BY DATE(tempahanCreatedDate)
  //       ORDER BY DATE(tempahanCreatedDate) DESC
  //       LIMIT 10`);

  //   console.log("Param", jumlahJualanByDate);

  return [
    jumlahKutipan,
    jumlahKutipanByDate,
    jumlahJualan,
    jumlahJualanByDate,
    ibadahKorban,
    ibadahKorbanByDate,
    ibadahAkikah,
    ibadahAkikahByDate,
  ];
}

async function getOverviewAdmin(year) {
  let jumlahWakil = null;
  let jumlahEjenTransaksi = null;
  let jumlahEjenAktif = null;
  let jumlahPeserta = null;
  let jumlahBahagian = null;
  let jumlahBahagianTerjual = null;

  // console.log("hAHAHA", year)

  jumlahWakil = await knex
    .connect(`userWakil`)
    .count(`userWakilId`, { as: "jumlahWakil" })
    .where(`setupKorbanId`, year);

  jumlahEjenTransaksi = await knex
    .connect(`userEjen`)
    .countDistinct(`tempahan.userEjenId`, { as: "jumlahEjenTransaksi" })
    .join(`tempahan`, `userEjen.userEjenId`, `tempahan.userEjenId`)
    .join(`user`, `userEjen.userId`, `user.userId`)
    .where(`user.userStatus`, "Active")
    .andWhere(`userEjen.setupKorbanId`, year);

  jumlahEjenAktif = await knex
    .connect(`userEjen`)
    .count(`userEjenId`, { as: "jumlahEjenAktif" })
    .join(`user`, `userEjen.userId`, `user.userId`)
    .where(`user.userStatus`, "Active")
    .andWhere(`userEjen.setupKorbanId`, year);

  jumlahPeserta = await knex
    .connect(`wakilPeserta`)
    .count(`wakilPesertaId`, { as: `jumlahPeserta` })
    .where(`setupKorbanId`, year);

  jumlahBahagian = await knex
    .connect(`liveStokBahagian`)
    .count(`liveStokBahagianId`, { as: `jumlahBahagian` })
    .where(`liveStokBahagianStatus`, "Active")
    .andWhere(`liveStokBahagian.setupKorbanId`, year);

  jumlahBahagianTerjual = await knex
    .connect(`liveStokBahagian`)
    .count(`liveStokBahagianId`, { as: `jumlahBahagianTerjual` })
    .where(`liveStokBahagianStatus`, "Reserved")
    .andWhere(`liveStokBahagian.setupKorbanId`, year);

  return [
    jumlahWakil,
    jumlahEjenTransaksi,
    jumlahEjenAktif,
    jumlahPeserta,
    jumlahBahagian,
    jumlahBahagianTerjual,
  ];
}

async function getRevenueAreaChartAdmin(filter, year) {
  let result = null;



  if (filter == "Today") {
    result = await knex.connect.raw(
      `SELECT SUM(tempahanTotalPrice) AS jumlahKutipan, TIME_FORMAT(tempahanCreatedDate, '%h %p') AS date
        FROM tempahan
        WHERE tempahanStatus = 'Success'
        AND setupKorbanId = "` +
        year +
        `"
        AND tempahanCreatedDate BETWEEN Curdate() AND Now()
        GROUP BY date`
    );
  } else if (filter == "7d") {
    result = await knex.connect.raw(
      `SELECT SUM(tempahanTotalPrice) AS jumlahKutipan, DATE_FORMAT(tempahanCreatedDate, '%d %b') AS date
        FROM tempahan
        WHERE tempahanStatus = 'Success'
        AND setupKorbanId = "` +
        year +
        `"
        GROUP BY DATE(tempahanCreatedDate), date
        ORDER BY DATE(tempahanCreatedDate) DESC
        LIMIT 7`
    );
  } else if (filter == "15d") {
    result = await knex.connect.raw(
      `SELECT SUM(tempahanTotalPrice) AS jumlahKutipan, DATE_FORMAT(tempahanCreatedDate, '%d %b') AS date
        FROM tempahan
        WHERE tempahanStatus = 'Success'
        AND setupKorbanId = "` +
        year +
        `"
        GROUP BY DATE(tempahanCreatedDate), date
        ORDER BY DATE(tempahanCreatedDate) DESC
        LIMIT 15`
    );
  } else if (filter == "1m") {
    result = await knex.connect.raw(
      `SELECT SUM(tempahanTotalPrice) AS jumlahKutipan, DATE_FORMAT(tempahanCreatedDate, '%d %b') AS date
        FROM tempahan
        WHERE tempahanStatus = 'Success'
        AND setupKorbanId = "` +
        year +
        `"
        AND tempahanCreatedDate between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW()
        GROUP BY DATE(tempahanCreatedDate), date
        ORDER BY DATE(tempahanCreatedDate) ASC`
    );
  } else if (filter == "1y") {
    result = await knex.connect.raw(
      `SELECT SUM(tempahanTotalPrice) AS jumlahKutipan, DATE_FORMAT(tempahanCreatedDate, '%b %y') AS date
        FROM tempahan
        WHERE tempahanStatus = 'Success'
        AND setupKorbanId = "` +
        year +
        `"
        AND YEAR(tempahanCreatedDate) = YEAR(CURRENT_DATE())
        GROUP BY date
    `
    );
  }

  return result;
}

// ↓ SAMA KNEX BEZA NAMA FUNCTION ↓
async function getTargetsBarChartAdmin(jenisHaiwan2, year) {
  let result = null;

  result = await knex
    .connect(`liveStok`)
    .select(`implementer.implementerNegara`)
    .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
    .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
    .join(`implementer`, `liveStok.implementerId`, `implementer.implementerId`)
    .join(
      `implementerLokasiHaiwan`,
      `liveStok.implementerLokasiHaiwanId`,
      `implementerLokasiHaiwan.implementerLokasiHaiwanId`
    )
    .where(`liveStok.liveStokType`, jenisHaiwan2)
    .andWhere(`liveStok.setupKorbanId`, year)
    .groupBy(`implementer.implementerNegara`);

  return result;
}

async function getDonutChartAdmin(jenisHaiwan, year) {
  let result = null;
  //   console.log("DONUT", year)

  result = await knex
    .connect(`liveStok`)
    .select(`implementer.implementerNegara`)
    .sum(`liveStok.liveStokBilBahagian`, { as: "jumlahBahagian" })
    .sum(`liveStok.liveStokBilBahagianBaki`, { as: "jumlahBahagianBaki" })
    .join(`implementer`, `liveStok.implementerId`, `implementer.implementerId`)
    .join(
      `implementerLokasiHaiwan`,
      `liveStok.implementerLokasiHaiwanId`,
      `implementerLokasiHaiwan.implementerLokasiHaiwanId`
    )
    .where(`liveStok.liveStokType`, jenisHaiwan)
    .andWhere(`liveStok.setupKorbanId`, year)
    .groupBy(`implementer.implementerNegara`);

  return result;
}
// ↑ SAMA KNEX BEZA NAMA FUNCTION ↑

async function getLatestTempahanAdmin(year) {
  let result = null;

  //   console.log("tempahan", year)

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `tempahan.tempahanCreatedDate as masa`
    )
    .limit(6)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .where(`tempahan.setupKorbanId`, year)
    .orderBy(`tempahan.tempahanId`, "desc");

  return result;
}

// DASHBOARD EJEN
async function getStatChartEjen(userId) {
  let jumlahKutipan = null;
  let jumlahKutipanByDate = null;

  let jumlahJualan = null;
  let jumlahJualanByDate = null;

  let ibadahKorban = null;
  let ibadahKorbanByDate = null;

  let ibadahAkikah = null;
  let ibadahAkikahByDate = null;

  jumlahKutipan = await knex
    .connect(`tempahan`)
    .sum(`tempahan.tempahanTotalPrice`, { as: "jumlahKutipan" })
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`tempahan.tempahanStatus`, "Success");

  jumlahKutipanByDate = await knex.connect.raw(
    `SELECT SUM(t.tempahanTotalPrice) AS jumlahKutipan
    FROM tempahan t
    JOIN userEjen ue
    ON t.userEjenId = ue.userEjenId
    WHERE t.tempahanStatus = 'Success'
    AND ue.userId = ` +
      userId +
      `
    GROUP BY DATE(t.tempahanCreatedDate) 
    ORDER BY DATE(t.tempahanCreatedDate) DESC
    LIMIT 10`
  );

  //     jumlahJualan = await knex.connect(`tempahan`)
  //         .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahJualan' })
  //         .join(`ejenCommission`, `ejenCommission.tempahanId`, `tempahan.tempahanId`)
  //         .join(`userEjen`, `ejenCommission.userEjenId`, `userEjen.userEjenId`)
  //         .where(`userEjen.userId`, userId)
  //         .where(`ejenCommission.ejenCommissionStatus`, 'Success')

  //     jumlahJualanByDate = await knex.connect.raw(`SELECT COUNT(t.tempahanId) AS jumlahJualan
  //     FROM tempahan t
  //   JOIN userEjen ue
  //    ON t.userEjenId = ue.userEjenId
  //    WHERE t.tempahanStatus = 'Success'
  //    AND ue.userId = `+userId+`
  //    GROUP BY DATE(t.tempahanCreatedDate)
  //    ORDER BY DATE(t.tempahanCreatedDate) DESC
  //    LIMIT 10`)

  jumlahJualan = await knex
    .connect(`tempahan`)
    .count(`tempahan.tempahanId`, { as: "jumlahJualan" })
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`tempahan.tempahanStatus`, "Success");

  jumlahJualanByDate = await knex.connect.raw(
    `SELECT COUNT(t.tempahanId) AS jumlahJualan
        FROM tempahan t
        JOIN userEjen ue
        ON t.userEjenId = ue.userEjenId
        WHERE t.tempahanStatus = 'Success'
        AND ue.userId = ` +
      userId +
      `
        GROUP BY DATE(t.tempahanCreatedDate)
        ORDER BY DATE(t.tempahanCreatedDate) DESC
        LIMIT 10`
  );

  ibadahKorban = await knex
    .connect(`tempahan`)
    .count(`tempahan.tempahanId`, { as: "ibadahKorban" })
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`tempahan.tempahanStatus`, "Success")
    .where(`tempahan.tempahanIbadahType`, "Korban");

  ibadahKorbanByDate = await knex.connect.raw(
    `SELECT COUNT(t.tempahanId) AS ibadahKorban
    FROM tempahan t
  JOIN userEjen ue
   ON t.userEjenId = ue.userEjenId
    WHERE t.tempahanStatus = 'Success'
    AND t.tempahanIbadahType = 'Korban'
    AND ue.userId = ` +
      userId +
      `
    GROUP BY DATE(t.tempahanCreatedDate)
    ORDER BY DATE(t.tempahanCreatedDate) DESC
    LIMIT 10`
  );

  ibadahAkikah = await knex
    .connect(`tempahan`)
    .count(`tempahan.tempahanId`, { as: "ibadahAkikah" })
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`tempahan.tempahanStatus`, "Success")
    .where(`tempahan.tempahanIbadahType`, "Akiqah");

  ibadahAkikahByDate = await knex.connect.raw(
    `SELECT COUNT(t.tempahanId) AS ibadahAkikah
    FROM tempahan t
JOIN userEjen ue
ON t.userEjenId = ue.userEjenId
    WHERE t.tempahanStatus = 'Success'
    AND t.tempahanIbadahType = 'Akiqah'
    AND ue.userId = ` +
      userId +
      `
    GROUP BY DATE(t.tempahanCreatedDate)
    ORDER BY DATE(t.tempahanCreatedDate) ASC
    LIMIT 10`
  );

  return [
    jumlahKutipan,
    jumlahKutipanByDate,
    jumlahJualan,
    jumlahJualanByDate,
    ibadahKorban,
    ibadahKorbanByDate,
    ibadahAkikah,
    ibadahAkikahByDate,
  ];
}

async function getOverviewEjen(userId) {
  let jumlahWakil = null;
  let jumlahEjenAktif = null;
  let jumlahEjenTransaksi = null;

  jumlahWakil = await knex
    .connect(`tempahan`)
    .count(`userWakil.userWakilId`, { as: "jumlahWakil" })
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .where(`userEjen.userId`, userId);

  // jumlahEjenAktif = await knex.connect(`userEjen`)
  //     .count(`userEjenId`, { as: 'jumlahEjenAktif' })
  //     .join(`user`, `userEjen.userId`, `user.userId`)
  //     .where(`user.userStatus`, 'Active')

  jumlahEjenAktif = await knex
    .connect(`tempahan`)
    .sum(`ejenCommission.ejenCommissionValue`, { as: "jumlahEjenAktif" })
    .join(`ejenCommission`, `ejenCommission.tempahanId`, `tempahan.tempahanId`)
    .join(`userEjen`, `ejenCommission.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`ejenCommission.ejenCommissionStatus`, "Success");

  jumlahEjenTransaksi = await knex
    .connect(`tempahan`)
    .sum(`ejenCommission.ejenCommissionValue`, { as: "jumlahEjenTransaksi" })
    .join(`ejenCommission`, `ejenCommission.tempahanId`, `tempahan.tempahanId`)
    .join(`userEjen`, `ejenCommission.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .where(`ejenCommission.ejenCommissionStatus`, "Pending");

  return [jumlahWakil, jumlahEjenAktif, jumlahEjenTransaksi];
}

async function getRevenueAreaChartEjen(userId, filter) {
  let result = null;

  if (filter == "Today") {
    result = await knex.connect.raw(
      `SELECT SUM(ec.ejenCommissionValue) AS jumlahKomisyen, TIME_FORMAT(t.tempahanCreatedDate, '%h %p') AS date
        FROM tempahan t
        JOIN ejenCommission ec
        ON t.tempahanId = ec.tempahanId
        JOIN userEjen ue
        ON ec.userEjenId = ue.userEjenId
        WHERE ec.ejenCommissionStatus = 'Success'
        AND ue.userId = ` +
        userId +
        `
        AND t.tempahanCreatedDate BETWEEN Curdate() AND Now()
        GROUP BY date`
    );
  } else if (filter == "7d") {
    result = await knex.connect.raw(
      `SELECT SUM(ec.ejenCommissionValue) AS jumlahKomisyen, DATE_FORMAT(t.tempahanCreatedDate, '%d %b') AS date
        FROM tempahan t
        JOIN ejenCommission ec
        ON t.tempahanId = ec.tempahanId
        JOIN userEjen ue
        ON ec.userEjenId = ue.userEjenId
        WHERE ec.ejenCommissionStatus = 'Success'
        AND ue.userId = ` +
        userId +
        `
        GROUP BY DATE(t.tempahanCreatedDate), date
        ORDER BY DATE(t.tempahanCreatedDate) DESC
        LIMIT 7`
    );
  } else if (filter == "15d") {
    result = await knex.connect.raw(
      `SELECT SUM(ec.ejenCommissionValue) AS jumlahKomisyen, DATE_FORMAT(t.tempahanCreatedDate, '%d %b') AS date
        FROM tempahan t
        JOIN ejenCommission ec
        ON t.tempahanId = ec.tempahanId
        JOIN userEjen ue
        ON ec.userEjenId = ue.userEjenId
        WHERE ec.ejenCommissionStatus = 'Success'
        AND ue.userId = ` +
        userId +
        `
        GROUP BY DATE(t.tempahanCreatedDate), date
        ORDER BY DATE(t.tempahanCreatedDate) DESC
        LIMIT 15`
    );
  } else if (filter == "1m") {
    result = await knex.connect.raw(
      `SELECT SUM(ec.ejenCommissionValue) AS jumlahKomisyen, DATE_FORMAT(t.tempahanCreatedDate, '%d %b') AS date
        FROM tempahan t
         JOIN ejenCommission ec
         ON t.tempahanId = ec.tempahanId
         JOIN userEjen ue
         ON ec.userEjenId = ue.userEjenId
         WHERE ec.ejenCommissionStatus = 'Success'
         AND ue.userId = ` +
        userId +
        `
         AND t.tempahanCreatedDate between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW()
         GROUP BY DATE(t.tempahanCreatedDate), date
         ORDER BY DATE(t.tempahanCreatedDate) ASC`
    );
  } else if (filter == "1y") {
    result = await knex.connect.raw(
      `SELECT SUM(ec.ejenCommissionValue) AS jumlahKomisyen, DATE_FORMAT(t.tempahanCreatedDate, '%b %y') AS date
        FROM tempahan t
        JOIN ejenCommission ec
        ON t.tempahanId = ec.tempahanId
        JOIN userEjen ue
        ON ec.userEjenId = ue.userEjenId
        WHERE ec.ejenCommissionStatus = 'Success'
        AND ue.userId = ` +
        userId +
        `
    AND YEAR(t.tempahanCreatedDate) = YEAR(CURRENT_DATE())
        GROUP BY date
    `
    );
  }

  return result;
}

async function getTargetsBarChartEjen(userId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT td.tempahanDetailsHaiwanType, 
    SUM(if(td.tempahanDetailsBhgnType = 'Ekor' AND td.tempahanDetailsHaiwanType = 'Lembu' OR td.tempahanDetailsHaiwanType = 'Unta', 7, 1) * td.tempahanDetailsBhgnCount) AS jumlahBahagian
    FROM tempahan t
    join tempahanDetails td
    ON t.tempahanId = td.tempahanId
    join userEjen ue
    ON t.userEjenId = ue.userEjenId
    WHERE ue.userId = ` +
      userId +
      `
    AND t.tempahanStatus = 'Success'
    AND td.tempahanDetailsStatus = 'Success'
    GROUP BY td.tempahanDetailsHaiwanType`
  );

  return result[0];
}

async function getLatestTempahanEjen(userId) {
  let result = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `tempahan.tempahanCreatedDate as masa`
    )
    .limit(6)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .where(`userEjen.userId`, userId)
    .orderBy(`tempahan.tempahanId`, "desc");

  return result;
}

// TAK SIAP LAGI
async function getDonutChartEjen(userId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT td.tempahanDetailsHaiwanType, 
    SUM(if(td.tempahanDetailsBhgnType = 'Ekor' AND td.tempahanDetailsHaiwanType = 'Lembu' OR td.tempahanDetailsHaiwanType = 'Unta', 7, 1) * td.tempahanDetailsBhgnCount) AS jumlahBahagian
    FROM tempahan t
    join tempahanDetails td
    ON t.tempahanId = td.tempahanId
    join userEjen ue
    ON t.userEjenId = ue.userEjenId
    WHERE ue.userId = ` +
      userId +
      `
    AND t.tempahanStatus = 'Success'
    AND td.tempahanDetailsStatus = 'Success'
    GROUP BY td.tempahanDetailsHaiwanType`
  );

  return result[0];
}

module.exports = {
  getStatChartAdmin,
  getOverviewAdmin,
  getRevenueAreaChartAdmin,
  getTargetsBarChartAdmin,
  getDonutChartAdmin,
  getLatestTempahanAdmin,
  getStatChartEjen,
  getOverviewEjen,
  getRevenueAreaChartEjen,
  getTargetsBarChartEjen,
  getLatestTempahanEjen,
  getDonutChartEjen,
  getSetupKorbanId,
};
