const knex = require("../../connection.js");
const moment = require("moment");
const axios = require("axios");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function getTempahanAdmin(year, status) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  //   console.log("WOI", year);

  if (status == "All") {
    result = await knex
      .connect(`tempahan`)
      .select(
        `tempahan.tempahanId`,
        `us2.userFullname`,
        `us1.userFullname as namaEjen`,
        `us2.userPhoneNo`,
        `us2.userEmail`,
        `tempahan.tempahanNo`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanIbadahType`,
        `tempahan.tempahanTotalPrice`,
        `tempahan.tempahanStatus`,
        `resit.resitPaymentMethod`,
        `tempahan.tempahanCreatedDate as masa`,
        `tempahan.tempahanUpdatedDate`,
        knex.connect.raw("count(tempahanDetailsBhgnCount) as bahagian"),
        "tempahanDetailsBhgnType as haiwan"
      )
      .distinct(`tempahan.tempahanId`)
      .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
      .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
      .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
      .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
      .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
      .leftJoin(
        "tempahanDetails",
        "tempahanDetails.tempahanId",
        "tempahan.tempahanId"
      )
      .where(`tempahan.setupKorbanId`, year)
      .groupBy(
        `tempahan.tempahanId`,
        `us2.userFullname`,
        `us1.userFullname`,
        `us2.userPhoneNo`,
        `us2.userEmail`,
        `tempahan.tempahanNo`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanIbadahType`,
        `tempahan.tempahanTotalPrice`,
        `tempahan.tempahanStatus`,
        `resit.resitPaymentMethod`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanUpdatedDate`,
        "tempahanDetailsBhgnType"
      )
      .orderBy(`tempahan.tempahanUpdatedDate`, "desc");

    jumlahTransaksi = await knex
      .connect(`tempahan`)
      .count(`tempahanId`, { as: "jumlahTransaksi" })
      .where(`setupKorbanId`, year);

    jumlahJualan = await knex
      .connect(`tempahan`)
      .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
      .where(`tempahanStatus`, `Success`)
      .where(`setupKorbanId`, year);

    for (let i = 0; i < result.length; i++) {
      result[i].tempahanCreatedDate = moment(
        result[i].tempahanCreatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
      result[i].tempahanUpdatedDate = moment(
        result[i].tempahanUpdatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
    }
  } else {
    // result = await knex
    //   .connect(`tempahan`)
    //   .select(
    //     `tempahan.tempahanId`,
    //     `us2.userFullname`,
    //     `us1.userFullname as namaEjen`,
    //     `us2.userPhoneNo`,
    //     `us2.userEmail`,
    //     `tempahan.tempahanNo`,
    //     `tempahan.tempahanCreatedDate`,
    //     `tempahan.tempahanIbadahType`,
    //     `tempahan.tempahanTotalPrice`,
    //     `tempahan.tempahanStatus`,
    //     `resit.resitPaymentMethod`,
    //     `tempahan.tempahanCreatedDate as masa`,
    //     `tempahan.tempahanUpdatedDate`,
    //     "tempahanDetailsBhgnCount as bahagian",
    //     "tempahanDetailsBhgnType as haiwan"
    //   )
    //   .distinct(`tempahan.tempahanId`)
    //   .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    //   .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    //   .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    //   .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    //   .leftJoin(
    //     "tempahanDetails",
    //     "tempahanDetails.tempahanId",
    //     "tempahan.tempahanId"
    //   )
    //   .where(`tempahan.setupKorbanId`, year)
    //   .where("tempahan.tempahanStatus", status)
    //   .orderBy(`tempahan.tempahanUpdatedDate`, "desc");

    result = await knex
      .connect(`tempahan`)
      .select(
        `tempahan.tempahanId`,
        `us2.userFullname`,
        `us1.userFullname as namaEjen`,
        `us2.userPhoneNo`,
        `us2.userEmail`,
        `tempahan.tempahanNo`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanIbadahType`,
        `tempahan.tempahanTotalPrice`,
        `tempahan.tempahanStatus`,
        `resit.resitPaymentMethod`,
        `tempahan.tempahanCreatedDate as masa`,
        `tempahan.tempahanUpdatedDate`,
        knex.connect.raw("count(tempahanDetailsBhgnCount) as bahagian"),
        "tempahanDetailsBhgnType as haiwan"
      )
      .distinct(`tempahan.tempahanId`)
      .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
      .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
      .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
      .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
      .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
      .leftJoin(
        "tempahanDetails",
        "tempahanDetails.tempahanId",
        "tempahan.tempahanId"
      )
      .where(`tempahan.setupKorbanId`, year)
      .where("tempahan.tempahanStatus", status)
      .groupBy(
        `tempahan.tempahanId`,
        `us2.userFullname`,
        `us1.userFullname`,
        `us2.userPhoneNo`,
        `us2.userEmail`,
        `tempahan.tempahanNo`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanIbadahType`,
        `tempahan.tempahanTotalPrice`,
        `tempahan.tempahanStatus`,
        `resit.resitPaymentMethod`,
        `tempahan.tempahanCreatedDate`,
        `tempahan.tempahanUpdatedDate`,
        "tempahanDetailsBhgnType"
      )
      .orderBy(`tempahan.tempahanUpdatedDate`, "desc");

    jumlahTransaksi = await knex
      .connect(`tempahan`)
      .count(`tempahanId`, { as: "jumlahTransaksi" })
      .where(`setupKorbanId`, year)
      .where("tempahan.tempahanStatus", status);

    jumlahJualan = await knex
      .connect(`tempahan`)
      .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
      .where(`setupKorbanId`, year)
      .where("tempahan.tempahanStatus", status);

    for (let i = 0; i < result.length; i++) {
      result[i].tempahanCreatedDate = moment(
        result[i].tempahanCreatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
      result[i].tempahanUpdatedDate = moment(
        result[i].tempahanUpdatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
    }
  }

  //   console.log("RESULT");
  // console.log(result);
  //   console.log(jumlahTransaksi);
  //   console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminDateToFrom(fromDate, toDate, status) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  // result = await knex.connect(`tempahan`)
  //     .select(
  //         `tempahan.tempahanId`,
  //         `user.userFullname`,
  //         `user.userPhoneNo`,
  //         `user.userEmail`,
  //         `tempahan.tempahanNo`,
  //         `tempahan.tempahanCreatedDate`,
  //         `tempahan.tempahanIbadahType`,
  //         `tempahan.tempahanTotalPrice`,
  //         `tempahan.tempahanStatus`,
  //         `resit.resitPaymentMethod`,
  //         `tempahan.tempahanCreatedDate as masa`,
  //     )
  //     .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //     .join(`user`, `userWakil.userId`, `user.UserId`)
  //     .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
  //     .whereBetween('tempahan.tempahanCreatedDate', ['2022-06-01', '2022-06-02'])
  //     .orderBy(`tempahan.tempahanId`, 'desc')

  result = await knex.connect.raw(
    `SELECT
    DISTINCT(tempahan.tempahanId),
    us2.userFullname,
    us1.userFullname AS namaEjen,
    us2.userPhoneNo,
    us2.userEmail,
    tempahan.tempahanNo,
    tempahan.tempahanCreatedDate,
    tempahan.tempahanIbadahType,
    tempahan.tempahanTotalPrice,
    tempahan.tempahanStatus,
    resit.resitPaymentMethod,
    tempahan.tempahanCreatedDate as masa,
    tempahan.tempahanUpdatedDate,
    tempahanDetailsBhgnCount AS bahagian,
    tempahanDetailsBhgnType AS haiwan
FROM
    tempahan
    JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
    JOIN user us1 ON us1.userId = userEjen.userId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user us2 ON us2.userId = userWakil.userId
    LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
    LEFT JOIN tempahan ON tempahan.tempahanId = tempahan.tempahanId
WHERE
    (DATE(tempahan.tempahanCreatedDate) BETWEEN '` +
      fromDate +
      `' AND '` +
      toDate +
      `')
    
    
ORDER BY
    tempahan.tempahanUpdatedDate DESC`
  );

  // var date = `SELECT
  // tempahan.tempahanId,
  // user.userFullname,
  // user.userPhoneNo,
  // user.userEmail,
  // tempahan.tempahanNo,
  // tempahan.tempahanCreatedDate,
  // tempahan.tempahanIbadahType,
  // tempahan.tempahanTotalPrice,
  // tempahan.tempahanStatus,
  // resit.resitPaymentMethod,
  // tempahan.tempahanCreatedDate as masa
  // FROM
  // tempahan
  // JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  // JOIN user ON userWakil.userId = user.userId
  // LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
  // WHERE
  // (DATE(tempahan.tempahanCreatedDate) BETWEEN '2022-06-01' AND '2022-06-02')
  // ORDER BY tempahan.tempahanId DESC`

  // result = await knex.connect.raw(date)

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" });

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result[0].length; i++) {
    result[0][i].tempahanCreatedDate = moment(
      result[0][i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[0][i].tempahanUpdatedDate = moment(
      result[0][i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log("result");
  console.log(result[0]);
  // console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminByStatusDate(fromDate, toDate, status) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  //   result = await knex.connect.raw(
  //     `SELECT
  //     DISTINCT(tempahan.tempahanId),
  //     us2.userFullname,
  //     us1.userFullname AS namaEjen,
  //     us2.userPhoneNo,
  //     us2.userEmail,
  //     tempahan.tempahanNo,
  //     tempahan.tempahanCreatedDate,
  //     tempahan.tempahanIbadahType,
  //     tempahan.tempahanTotalPrice,
  //     tempahan.tempahanStatus,
  //     resit.resitPaymentMethod,
  //     tempahan.tempahanCreatedDate as masa,
  //     tempahan.tempahanUpdatedDate,
  //     tempahanDetailsBhgnCount AS bahagian,
  //     tempahanDetailsBhgnType AS haiwan
  // FROM
  //     tempahan
  //     JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
  //     JOIN user us1 ON us1.userId = userEjen.userId
  //     JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  //     JOIN user us2 ON us2.userId = userWakil.userId
  //     LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
  //     LEFT JOIN tempahan ON tempahan.tempahanId = tempahan.tempahanId
  // WHERE
  //     (DATE(tempahan.tempahanCreatedDate) BETWEEN '` +
  //       fromDate +
  //       `' AND '` +
  //       toDate +
  //       `')
  //     AND tempahan.tempahanStatus = '` +
  //       status +
  //       `'
  // ORDER BY
  //     tempahan.tempahanUpdatedDate DESC`
  //   );

  if (status == "All") {
    // result = await knex
    //   .connect("tempahan")
    //   .select(
    //     "tempahan.tempahanId",
    //     "us2.userFullname",
    //     "us1.userFullname as namaEjen",
    //     "us2.userPhoneNo",
    //     "us2.userEmail",
    //     "tempahan.tempahanNo",
    //     "tempahan.tempahanCreatedDate",
    //     "tempahan.tempahanIbadahType",
    //     "tempahan.tempahanTotalPrice",
    //     "tempahan.tempahanStatus",
    //     "resit.resitPaymentMethod",
    //     "tempahan.tempahanCreatedDate as masa",
    //     "tempahan.tempahanUpdatedDate",
    //     "tempahanDetailsBhgnCount as bahagian",
    //     "tempahanDetailsBhgnType as haiwan"
    //   )
    //   .distinct("tempahan.tempahanId")
    //   .join("userEjen", "tempahan.userEjenId", "userEjen.userEjenId")
    //   .join("user AS us1", "us1.userId", "userEjen.userId")
    //   .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    //   .join("user AS us2", "us2.userId", "userWakil.userId")
    //   .leftJoin("resit", "resit.tempahanId", "tempahan.tempahanId")
    //   .leftJoin(
    //     "tempahanDetails",
    //     "tempahanDetails.tempahanId",
    //     "tempahan.tempahanId"
    //   )
    //   .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
    //   .orderBy("tempahan.tempahanUpdatedDate", "desc");

    result = await knex
      .connect("tempahan")
      .select(
        "tempahan.tempahanId",
        "us2.userFullname",
        "us1.userFullname as namaEjen",
        "us2.userPhoneNo",
        "us2.userEmail",
        "tempahan.tempahanNo",
        "tempahan.tempahanCreatedDate",
        "tempahan.tempahanIbadahType",
        "tempahan.tempahanTotalPrice",
        "tempahan.tempahanStatus",
        "resit.resitPaymentMethod",
        "tempahan.tempahanCreatedDate as masa",
        "tempahan.tempahanUpdatedDate",
        knex.connect.raw("COUNT(tempahanDetailsBhgnCount) as bahagian"),
        "tempahanDetailsBhgnType as haiwan"
      )
      .distinct("tempahan.tempahanId")
      .join("userEjen", "tempahan.userEjenId", "userEjen.userEjenId")
      .join("user AS us1", "us1.userId", "userEjen.userId")
      .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
      .join("user AS us2", "us2.userId", "userWakil.userId")
      .leftJoin("resit", "resit.tempahanId", "tempahan.tempahanId")
      .leftJoin(
        "tempahanDetails",
        "tempahanDetails.tempahanId",
        "tempahan.tempahanId"
      )
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
      .groupBy(
        "tempahan.tempahanId",
        "us2.userFullname",
        "us1.userFullname",
        "us2.userPhoneNo",
        "us2.userEmail",
        "tempahan.tempahanNo",
        "tempahan.tempahanCreatedDate",
        "tempahan.tempahanIbadahType",
        "tempahan.tempahanTotalPrice",
        "tempahan.tempahanStatus",
        "resit.resitPaymentMethod",
        "masa",
        "tempahan.tempahanUpdatedDate",
        "tempahanDetailsBhgnType"
      )
      .orderBy("tempahan.tempahanUpdatedDate", "desc");

    jumlahTransaksi = await knex
      .connect(`tempahan`)
      .count(`tempahanId`, { as: "jumlahTransaksi" })
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate]);

    jumlahJualan = await knex
      .connect(`tempahan`)
      .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
      .where(`tempahanStatus`, `Success`)
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate]);

    for (let i = 0; i < result[0].length; i++) {
      result[0][i].tempahanCreatedDate = moment(
        result[0][i].tempahanCreatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
      result[0][i].tempahanUpdatedDate = moment(
        result[0][i].tempahanUpdatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
    }
  } else {
    // result = await knex
    //   .connect("tempahan")
    //   .select(
    //     "tempahan.tempahanId",
    //     "us2.userFullname",
    //     "us1.userFullname as namaEjen",
    //     "us2.userPhoneNo",
    //     "us2.userEmail",
    //     "tempahan.tempahanNo",
    //     "tempahan.tempahanCreatedDate",
    //     "tempahan.tempahanIbadahType",
    //     "tempahan.tempahanTotalPrice",
    //     "tempahan.tempahanStatus",
    //     "resit.resitPaymentMethod",
    //     "tempahan.tempahanCreatedDate as masa",
    //     "tempahan.tempahanUpdatedDate",
    //     "tempahanDetailsBhgnCount as bahagian",
    //     "tempahanDetailsBhgnType as haiwan"
    //   )
    //   .distinct("tempahan.tempahanId")
    //   .join("userEjen", "tempahan.userEjenId", "userEjen.userEjenId")
    //   .join("user AS us1", "us1.userId", "userEjen.userId")
    //   .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    //   .join("user AS us2", "us2.userId", "userWakil.userId")
    //   .leftJoin("resit", "resit.tempahanId", "tempahan.tempahanId")
    //   .leftJoin(
    //     "tempahanDetails",
    //     "tempahanDetails.tempahanId",
    //     "tempahan.tempahanId"
    //   )
    //   .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
    //   .where("tempahan.tempahanStatus", status)
    //   .orderBy("tempahan.tempahanUpdatedDate", "desc");

    result = await knex
      .connect("tempahan")
      .select(
        "tempahan.tempahanId",
        "us2.userFullname",
        "us1.userFullname as namaEjen",
        "us2.userPhoneNo",
        "us2.userEmail",
        "tempahan.tempahanNo",
        "tempahan.tempahanCreatedDate",
        "tempahan.tempahanIbadahType",
        "tempahan.tempahanTotalPrice",
        "tempahan.tempahanStatus",
        "resit.resitPaymentMethod",
        "tempahan.tempahanCreatedDate as masa",
        "tempahan.tempahanUpdatedDate",
        knex.connect.raw("COUNT(tempahanDetailsBhgnCount) as bahagian"),
        "tempahanDetailsBhgnType as haiwan"
      )
      .distinct("tempahan.tempahanId")
      .join("userEjen", "tempahan.userEjenId", "userEjen.userEjenId")
      .join("user AS us1", "us1.userId", "userEjen.userId")
      .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
      .join("user AS us2", "us2.userId", "userWakil.userId")
      .leftJoin("resit", "resit.tempahanId", "tempahan.tempahanId")
      .leftJoin(
        "tempahanDetails",
        "tempahanDetails.tempahanId",
        "tempahan.tempahanId"
      )
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
      .where("tempahan.tempahanStatus", status)
      .groupBy(
        "tempahan.tempahanId",
        "us2.userFullname",
        "us1.userFullname",
        "us2.userPhoneNo",
        "us2.userEmail",
        "tempahan.tempahanNo",
        "tempahan.tempahanCreatedDate",
        "tempahan.tempahanIbadahType",
        "tempahan.tempahanTotalPrice",
        "tempahan.tempahanStatus",
        "resit.resitPaymentMethod",
        "masa",
        "tempahan.tempahanUpdatedDate",
        "tempahanDetailsBhgnType"
      )
      .orderBy("tempahan.tempahanUpdatedDate", "desc");

    jumlahTransaksi = await knex
      .connect(`tempahan`)
      .count(`tempahanId`, { as: "jumlahTransaksi" })
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
      .where("tempahan.tempahanStatus", status);

    jumlahJualan = await knex
      .connect(`tempahan`)
      .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
      .whereBetween("tempahan.tempahanCreatedDate", [fromDate, toDate])
      .where("tempahan.tempahanStatus", status);

    for (let i = 0; i < result[0].length; i++) {
      result[0][i].tempahanCreatedDate = moment(
        result[0][i].tempahanCreatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
      result[0][i].tempahanUpdatedDate = moment(
        result[0][i].tempahanUpdatedDate
      ).format("YYYY-MM-DD HH:mm:ss");
    }
  }

  console.log("result");
  // console.log(result);
  // console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminDateToFromPending(fromDate, toDate) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  // result = await knex.connect(`tempahan`)
  //     .select(
  //         `tempahan.tempahanId`,
  //         `user.userFullname`,
  //         `user.userPhoneNo`,
  //         `user.userEmail`,
  //         `tempahan.tempahanNo`,
  //         `tempahan.tempahanCreatedDate`,
  //         `tempahan.tempahanIbadahType`,
  //         `tempahan.tempahanTotalPrice`,
  //         `tempahan.tempahanStatus`,
  //         `resit.resitPaymentMethod`,
  //         `tempahan.tempahanCreatedDate as masa`,
  //     )
  //     .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //     .join(`user`, `userWakil.userId`, `user.UserId`)
  //     .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
  //     .whereBetween('tempahan.tempahanCreatedDate', ['2022-06-01', '2022-06-02'])
  //     .orderBy(`tempahan.tempahanId`, 'desc')

  result = await knex.connect.raw(
    `SELECT
    DISTINCT(tempahan.tempahanId),
    us2.userFullname,
    us1.userFullname AS namaEjen,
    us2.userPhoneNo,
    us2.userEmail,
    tempahan.tempahanNo,
    tempahan.tempahanCreatedDate,
    tempahan.tempahanIbadahType,
    tempahan.tempahanTotalPrice,
    tempahan.tempahanStatus,
    resit.resitPaymentMethod,
    tempahan.tempahanCreatedDate as masa,
    tempahan.tempahanUpdatedDate,
    tempahanDetailsBhgnCount AS bahagian,
    tempahanDetailsBhgnType AS haiwan
FROM
    tempahan
    JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
    JOIN user us1 ON us1.userId = userEjen.userId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user us2 ON us2.userId = userWakil.userId
    LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
    LEFT JOIN tempahan ON tempahan.tempahanId = tempahan.tempahanId
WHERE
    (DATE(tempahan.tempahanCreatedDate) BETWEEN '` +
      fromDate +
      `' AND '` +
      toDate +
      `')
    AND tempahan.tempahanStatus = 'Pending Payment'
ORDER BY
    tempahan.tempahanUpdatedDate DESC`
  );

  // var date = `SELECT
  // tempahan.tempahanId,
  // user.userFullname,
  // user.userPhoneNo,
  // user.userEmail,
  // tempahan.tempahanNo,
  // tempahan.tempahanCreatedDate,
  // tempahan.tempahanIbadahType,
  // tempahan.tempahanTotalPrice,
  // tempahan.tempahanStatus,
  // resit.resitPaymentMethod,
  // tempahan.tempahanCreatedDate as masa
  // FROM
  // tempahan
  // JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  // JOIN user ON userWakil.userId = user.userId
  // LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
  // WHERE
  // (DATE(tempahan.tempahanCreatedDate) BETWEEN '2022-06-01' AND '2022-06-02')
  // ORDER BY tempahan.tempahanId DESC`

  // result = await knex.connect.raw(date)

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`tempahan.tempahanStatus`, "Pending Payment");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result[0].length; i++) {
    result[0][i].tempahanCreatedDate = moment(
      result[0][i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[0][i].tempahanUpdatedDate = moment(
      result[0][i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log("result");
  console.log(result[0]);
  // console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminByDateAsc() {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      `tempahanDetailsBhgnCount as bahagian`,
      `tempahanDetailsBhgnType as haiwan`
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(`tempahan`, `tempahan.tempahanId`, `tempahan.tempahanId`)
    .orderBy(`tempahan.tempahanUpdatedDate`, "asc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" });

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminPendingPaymentAsc() {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      `tempahanDetailsBhgnCount as bahagian`,
      `tempahanDetailsBhgnType as haiwan`
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(`tempahan`, `tempahan.tempahanId`, `tempahan.tempahanId`)
    .orderBy(`tempahan.tempahanUpdatedDate`, "asc")
    .where(`tempahan.tempahanStatus`, "Pending Payment");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`tempahan.tempahanStatus`, "Pending Payment");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminSuccess(year) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.setupKorbanId`, year)
    .where(`tempahan.tempahanStatus`, "Success")
    .where(`resit.resitPaymentStatus`, "Success")
    .orderBy(`tempahan.tempahanCreatedDate`, "desc");

  // result = await knex.connect.raw(`
  // SELECT
  // tempahan.tempahanId,
  // us2.userFullname,
  // us1.userFullname as namaEjen,
  // us2.userPhoneNo,us2.userEmail,
  // tempahan.tempahanNo,
  // tempahan.tempahanCreatedDate,
  // tempahan.tempahanIbadahType,
  // tempahan.tempahanTotalPrice,
  // tempahan.tempahanStatus,
  // resit.resitPaymentMethod,
  // tempahan.tempahanCreatedDate as masa
  // FROM tempahan
  // JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
  // JOIN user AS us1 ON us1.userId = userEjen.userId
  // JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  // JOIN user AS us2 ON us2.userId = userWakil.userId
  // JOIN resit ON resit.tempahanId = tempahan.tempahanId
  // WHERE tempahan.tempahanStatus = 'Success'
  // and resit.resitPaymentStatus = "Success"
  // ORDER BY tempahan.tempahanUpdatedDate DESC`)

  jumlahTransaksi = await knex.connect.raw(
    `select COUNT(tempahan.tempahanId) AS jumlahTransaksi FROM tempahan JOIN resit ON resit.tempahanId = tempahan.tempahanId WHERE tempahanStatus = 'Success' AND resit.resitPaymentStatus = 'Success'`
  );

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminFailed(year) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.tempahanStatus`, "Failed")
    .where(`resit.resitPaymentStatus`, "Failed")
    .orderBy(`tempahan.tempahanCreatedDate`, "desc");

  // result = await knex.connect.raw(`
  // SELECT
  // tempahan.tempahanId,
  // us2.userFullname,
  // us1.userFullname as namaEjen,
  // us2.userPhoneNo,us2.userEmail,
  // tempahan.tempahanNo,
  // tempahan.tempahanCreatedDate,
  // tempahan.tempahanIbadahType,
  // tempahan.tempahanTotalPrice,
  // tempahan.tempahanStatus,
  // resit.resitPaymentMethod,
  // tempahan.tempahanCreatedDate as masa
  // FROM tempahan
  // JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
  // JOIN user AS us1 ON us1.userId = userEjen.userId
  // JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  // JOIN user AS us2 ON us2.userId = userWakil.userId
  // JOIN resit ON resit.tempahanId = tempahan.tempahanId
  // WHERE tempahan.tempahanStatus = 'Failed'
  // and resit.resitPaymentStatus = 'Failed'
  // ORDER BY tempahan.tempahanUpdatedDate DESC`)

  jumlahTransaksi = await knex.connect.raw(
    `select COUNT(tempahan.tempahanId) AS jumlahTransaksi FROM tempahan JOIN resit ON resit.tempahanId = tempahan.tempahanId WHERE tempahanStatus = 'Failed' AND resit.resitPaymentStatus = 'Failed'`
  );

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminPendingOrder(year) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.tempahanStatus`, "Pending Order")
    .orderBy(`tempahan.tempahanCreatedDate`, "desc");

  // result = await knex.connect.raw(`
  // SELECT
  // tempahan.tempahanId,
  // us2.userFullname,
  // us1.userFullname as namaEjen,
  // us2.userPhoneNo,us2.userEmail,
  // tempahan.tempahanNo,
  // tempahan.tempahanCreatedDate,
  // tempahan.tempahanIbadahType,
  // tempahan.tempahanTotalPrice,
  // tempahan.tempahanStatus,
  // resit.resitPaymentMethod,
  // tempahan.tempahanCreatedDate as masa
  // FROM tempahan
  // JOIN userEjen ON tempahan.userEjenId = userEjen.userEjenId
  // JOIN user AS us1 ON us1.userId = userEjen.userId
  // JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
  // JOIN user AS us2 ON us2.userId = userWakil.userId
  // LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
  // WHERE tempahan.tempahanStatus = 'Pending Order'
  // ORDER BY tempahan.tempahanUpdatedDate DESC`)

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`tempahan.tempahanStatus`, "Pending Order");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminPendingPayment(year) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.tempahanStatus`, "Pending Payment")
    .andWhere(`tempahan.setupKorbanId`, year)
    .orderBy(`tempahan.tempahanCreatedDate`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`tempahanStatus`, "Pending Payment")
    .andWhere(`setupKorbanId`, year);

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, "Pending Payment")
    .andWhere(`setupKorbanId`, year);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  //   console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminCancelOrder(year) {
  let result = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.tempahanStatus`, "Cancel Order")
    .orderBy(`tempahan.tempahanCreatedDate`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`tempahan.tempahanStatus`, "Cancel Order");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`tempahanStatus`, `Success`);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(result);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [result, jumlahTransaksi, jumlahJualan];
}

async function getTempahanAdminByStatus(status) {
  let result = null;

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `us2.userFullname`,
      `us1.userFullname as namaEjen`,
      `us2.userPhoneNo`,
      `us2.userEmail`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanUpdatedDate`,
      "tempahanDetailsBhgnCount as bahagian",
      "tempahanDetailsBhgnType as haiwan"
    )
    .distinct(`tempahan.tempahanId`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user AS us1`, `us1.userId`, `userEjen.userId`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user AS us2`, `us2.userId`, `userWakil.userId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "tempahan.tempahanId"
    )
    .where(`tempahan.tempahanStatus`, status)
    .orderBy(`tempahan.tempahanUpdatedDate`, "desc");

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
    result[i].tempahanUpdatedDate = moment(
      result[i].tempahanUpdatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  return result;
}

async function getTempahanEjen(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId);

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Success");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenDateToFrom(userId, fromDate, toDate) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex.connect.raw(
    `SELECT
        tempahan.tempahanId,
        user.userFullname,
        user.userPhoneNo,
        user.userEmail,
        ejenCommission.ejenCommissionValue,
        tempahan.tempahanNo,
        tempahan.tempahanCreatedDate,
        tempahan.tempahanIbadahType,
        tempahan.tempahanTotalPrice,
        tempahan.tempahanStatus,
        resit.resitPaymentMethod,
        tempahan.tempahanCreatedDate as masa
        FROM
        tempahan
        JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
        JOIN user ON userWakil.userId = user.userId
        LEFT JOIN resit ON resit.tempahanId = tempahan.tempahanId
        LEFT JOIN ejenCommission ON tempahan.tempahanId = ejenCommission.tempahanId
        WHERE tempahan.userEjenId = '` +
      resultEjen[0].userEjenId +
      `'
        AND
        (DATE(tempahan.tempahanCreatedDate) BETWEEN '` +
      fromDate +
      `' AND '` +
      toDate +
      `')
        ORDER BY tempahan.tempahanId DESC`
  );

  // resultTempahan = await knex.connect(`tempahan`)
  //     .select(
  //         `tempahan.tempahanId`,
  //         `user.userFullname`,
  //         `user.userPhoneNo`,
  //         `user.userEmail`,
  //         `ejenCommission.ejenCommissionValue`,
  //         `tempahan.tempahanNo`,
  //         `tempahan.tempahanCreatedDate`,
  //         `tempahan.tempahanIbadahType`,
  //         `tempahan.tempahanTotalPrice`,
  //         `tempahan.tempahanStatus`,
  //         `resit.resitPaymentMethod`,
  //     )
  //     .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //     .join(`user`, `userWakil.userId`, `user.UserId`)
  //     .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
  //     .leftJoin(`ejenCommission`, `tempahan.tempahanId`, `ejenCommission.tempahanId`)
  //     .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
  //     .whereBetween(`tempahan.tempahanCreatedDate`,[fromDate,toDate])
  //     .orderBy(`tempahan.tempahanId`, 'desc')

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId);

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Success");

  for (let i = 0; i < resultTempahan[0].length; i++) {
    resultTempahan[0][i].tempahanCreatedDate = moment(
      resultTempahan[0][i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log("DATE FROM");
  console.log(resultTempahan[0]);
  console.log("DATE FROM");
  // console.log(resultEjen);
  // console.log(resultTempahan);
  // console.log(jumlahTransaksi);
  // console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenAsc(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanStatus`,
      `resit.resitPaymentMethod`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .orderBy(`tempahan.tempahanId`, "asc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId);

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Success");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenSuccess(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, "Success")
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Success");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Success");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenFailed(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanCreatedDate as masa`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, "Failed")
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Failed");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Failed");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenPendingOrder(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .leftJoin(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, "Pending Order")
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Pending Order");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Pending Order");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenPendingPayment(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, "Pending Payment")
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Pending Payment");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Pending Payment");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenCancelOrder(userId) {
  let resultEjen = null;
  let resultTempahan = null;
  let jumlahTransaksi = null;
  let jumlahJualan = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  resultTempahan = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, "Cancel Order")
    .orderBy(`tempahan.tempahanId`, "desc");

  jumlahTransaksi = await knex
    .connect(`tempahan`)
    .count(`tempahanId`, { as: "jumlahTransaksi" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Cancel Order");

  jumlahJualan = await knex
    .connect(`tempahan`)
    .sum(`tempahanTotalPrice`, { as: "jumlahJualan" })
    .where(`userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahanStatus`, "Cancel Order");

  for (let i = 0; i < resultTempahan.length; i++) {
    resultTempahan[i].tempahanCreatedDate = moment(
      resultTempahan[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(resultEjen);
  console.log(resultTempahan);
  console.log(jumlahTransaksi);
  console.log(jumlahJualan);

  return [resultEjen, resultTempahan, jumlahTransaksi, jumlahJualan];
}

async function getTempahanEjenByStatus(userId, status) {
  let result = null;
  let resultEjen = null;

  resultEjen = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  result = await knex
    .connect(`tempahan`)
    .select(
      `tempahan.tempahanId`,
      `user.userFullname`,
      `user.userPhoneNo`,
      `user.userEmail`,
      `ejenCommission.ejenCommissionValue`,
      `tempahan.tempahanNo`,
      `tempahan.tempahanCreatedDate`,
      `tempahan.tempahanIbadahType`,
      `tempahan.tempahanTotalPrice`,
      `tempahan.tempahanCreatedDate as masa`,
      `resit.resitPaymentMethod`,
      `tempahan.tempahanStatus`
    )
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .join(`resit`, `resit.tempahanId`, `tempahan.tempahanId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.userEjenId`, resultEjen[0].userEjenId)
    .where(`tempahan.tempahanStatus`, status);

  for (let i = 0; i < result.length; i++) {
    result[i].tempahanCreatedDate = moment(
      result[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  return result;
}

async function getButiranTempahanAdmin(tempahanId) {
  let tempahanDanWakil = null;
  let resit = null;
  let ejen = null;
  let tempahanDetails = null;

  tempahanDanWakil = await knex
    .connect(`tempahan`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .where(`tempahan.tempahanId`, tempahanId);

  resit = await knex.connect(`resit`).where(`tempahanId`, tempahanId);

  ejen = await knex
    .connect(`tempahan`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user`, `user.userId`, `userEjen.userId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.tempahanId`, tempahanId);

  tempahanDetails = await knex
    .connect(`tempahanDetails`)
    .select(
      `tempahanDetails.tempahanDetailsId`,
      `tempahanDetails.tempahanDetailsHaiwanType`,
      `tempahanDetails.tempahanDetailsBhgnCount`,
      `tempahanDetails.tempahanDetailsBhgnType`,
      `liveStok.liveStokNo`,
      `wakilPeserta.wakilPesertaName`,
      `tempahanDetails.tempahanDetailsPrice`,
      `implementer.implementerNegara`
    )
    .distinct(`tempahanDetails.tempahanDetailsId`)
    .leftJoin(
      `tempahanDetailsLiveStok`,
      `tempahanDetails.tempahanDetailsId`,
      `tempahanDetailsLiveStok.tempahanDetailsId`
    )
    .leftJoin(
      `liveStok`,
      `tempahanDetailsLiveStok.liveStokId`,
      `liveStok.liveStokId`
    )
    .leftJoin(
      `implementer`,
      `tempahanDetails.implementerId`,
      `implementer.implementerId`
    )
    .leftJoin(
      `wakilPeserta`,
      `tempahanDetails.wakilPesertaId`,
      `wakilPeserta.wakilPesertaId`
    )
    .where(`tempahanDetails.tempahanId`, tempahanId);

  // console.log('tempahanDetails =>> ');
  // console.log(tempahanDetails);
  // console.log(resit);
  // console.log(ejen);
  // console.log(tempahanDetails);

  let liveStock = await knex
    .connect("tempahanDetailsLiveStok")
    .select("wakilPesertaName as WAKIL", "liveStokBahagianId as LIVE_STOK")
    .leftJoin(
      "wakilPeserta",
      "wakilPeserta.wakilPesertaId",
      "tempahanDetailsLiveStok.wakilPesertaId"
    )
    .where("tempahanId", tempahanId);

  return [tempahanDanWakil, resit, ejen, tempahanDetails, liveStock];
}

async function getButiranTempahanEjen(tempahanId) {
  let tempahanDanWakil = null;
  let resit = null;
  let ejen = null;
  let tempahanDetails = null;

  tempahanDanWakil = await knex
    .connect(`tempahan`)
    .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
    .join(`user`, `userWakil.userId`, `user.UserId`)
    .where(`tempahan.tempahanId`, tempahanId);

  resit = await knex.connect(`resit`).where(`tempahanId`, tempahanId);

  ejen = await knex
    .connect(`tempahan`)
    .join(`userEjen`, `tempahan.userEjenId`, `userEjen.userEjenId`)
    .join(`user`, `user.userId`, `userEjen.userId`)
    .leftJoin(
      `ejenCommission`,
      `tempahan.tempahanId`,
      `ejenCommission.tempahanId`
    )
    .where(`tempahan.tempahanId`, tempahanId);

  tempahanDetails = await knex
    .connect(`tempahanDetails`)
    .select(
      `tempahanDetails.tempahanDetailsId`,
      `tempahanDetails.tempahanDetailsHaiwanType`,
      `tempahanDetails.tempahanDetailsBhgnCount`,
      `tempahanDetails.tempahanDetailsBhgnType`,
      `liveStok.liveStokNo`,
      `wakilPeserta.wakilPesertaName`,
      `tempahanDetails.tempahanDetailsPrice`,
      `implementer.implementerNegara`
    )
    .distinct(`tempahanDetails.tempahanDetailsId`)
    .leftJoin(
      `tempahanDetailsLiveStok`,
      `tempahanDetails.tempahanDetailsId`,
      `tempahanDetailsLiveStok.tempahanDetailsId`
    )
    .leftJoin(
      `liveStok`,
      `tempahanDetailsLiveStok.liveStokId`,
      `liveStok.liveStokId`
    )
    .leftJoin(
      `implementer`,
      `tempahanDetails.implementerId`,
      `implementer.implementerId`
    )
    .leftJoin(
      `wakilPeserta`,
      `tempahanDetailsLiveStok.wakilPesertaId`,
      `wakilPeserta.wakilPesertaId`
    )
    .where(`tempahanDetails.tempahanId`, tempahanId);

  console.log(tempahanDanWakil);
  console.log(resit);
  console.log(ejen);

  return [tempahanDanWakil, resit, ejen, tempahanDetails];
}

async function getTempahanDetailsLiveStok(tempahanDetailsId) {
  let result = null;

  result = await knex
    .connect(`tempahanDetails`)
    .join(
      `tempahanDetailsLiveStok`,
      `tempahanDetails.tempahanDetailsId`,
      `tempahanDetailsLiveStok.tempahanDetailsId`
    )
    .join(
      `liveStokBahagian`,
      `tempahanDetailsLiveStok.liveStokBahagianId`,
      `liveStokBahagian.liveStokBahagianId`
    )
    .where(`tempahanDetails.tempahanDetailsId`, tempahanDetailsId);

  console.log(result);

  return result;
}

async function getTempahanImplementer(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .orderBy(`tempahan.tempahanCreatedDate`, "desc");
  // .orderBy(`liveStok.liveStokNo`, 'desc')
  // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId);

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerAsc(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .orderBy(`tempahan.tempahanCreatedDate`, "asc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .orderBy("liveStok.liveStokNo", "asc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId);

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerSuccess(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, "Success")
  //   // .orderBy(`tempahan.tempahanCreatedDate`, 'desc')
  //   .orderBy(`liveStok.liveStokNo`, "desc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", "Success")
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, "Success");

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerFailed(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, "Failed")
  //   // .orderBy(`tempahan.tempahanCreatedDate`, 'desc')
  //   .orderBy(`liveStok.liveStokNo`, "desc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", "Failed")
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, "Failed");

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerPendingOrder(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, "Pending Order")
  //   // .orderBy(`tempahan.tempahanCreatedDate`, 'desc')
  //   .orderBy(`liveStok.liveStokNo`, "desc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", "Pending Order")
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, "Pending Order");

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerPendingPayment(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, "Pending Payment")
  //   // .orderBy(`tempahan.tempahanCreatedDate`, 'desc')
  //   .orderBy(`liveStok.liveStokNo`, "desc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", "Pending Payment")
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, "Pending Payment");

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerCancelOrder(implementerId) {
  let implementer = null;
  let resultTempahanDetails = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, "Cancel Order")
  //   // .orderBy(`tempahan.tempahanCreatedDate`, 'desc')
  //   .orderBy(`liveStok.liveStokNo`, "desc");
  // // .where(`tempahan.tempahanStatus`, `Success`)

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", "Cancel Order")
    .orderBy("liveStok.liveStokNo", "desc");

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, "Cancel Order");

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getTempahanImplementerByStatus(implementerId, status) {
  let resultTempahanDetails = null;
  let implementer = null;
  let jumlahTransaksi = null;

  implementer = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  // resultTempahanDetails = await knex
  //   .connect(`tempahanDetails`)
  //   .select(
  //     `tempahanDetails.tempahanDetailsId`,
  //     `wakilPeserta.wakilPesertaName`,
  //     `user.userFullname`,
  //     `user.userPhoneNo`,
  //     `user.userEmail`,
  //     `tempahan.tempahanNo`,
  //     `tempahan.tempahanCreatedDate`,
  //     `tempahan.tempahanIbadahType`,
  //     `tempahanDetails.tempahanDetailsHaiwanType`,
  //     `tempahanDetails.tempahanDetailsBhgnCount`,
  //     `tempahanDetails.tempahanDetailsBhgnType`,
  //     `liveStok.liveStokNo`,
  //     `tempahan.tempahanStatus`
  //   )
  //   .distinct(`tempahanDetails.tempahanDetailsId`)
  //   .join(`tempahan`, `tempahanDetails.tempahanId`, `tempahan.tempahanId`)
  //   .join(`userWakil`, `tempahan.userWakilId`, `userWakil.userWakilId`)
  //   .join(`user`, `userWakil.userId`, `user.UserId`)
  //   .leftJoin(
  //     `tempahanDetailsLiveStok`,
  //     `tempahanDetails.tempahanDetailsId`,
  //     `tempahanDetailsLiveStok.tempahanDetailsId`
  //   )
  //   .leftJoin(
  //     `liveStok`,
  //     `tempahanDetailsLiveStok.liveStokId`,
  //     `liveStok.liveStokId`
  //   )
  //   .leftJoin(
  //     `wakilPeserta`,
  //     `tempahanDetails.wakilPesertaId`,
  //     `wakilPeserta.wakilPesertaId`
  //   )
  //   .where(`tempahanDetails.implementerId`, implementerId)
  //   .where(`tempahan.tempahanStatus`, status);

  resultTempahanDetails = await knex
    .connect("tempahanDetails")
    .select(
      "tempahan.tempahanId",
      "tempahanDetails.tempahanDetailsId",
      "wakilPeserta.wakilPesertaName",
      "user.userFullname",
      "user.userPhoneNo",
      "user.userEmail",
      "tempahan.tempahanNo",
      "tempahan.tempahanCreatedDate",
      "tempahan.tempahanIbadahType",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "liveStok.liveStokNo",
      "tempahan.tempahanStatus",
      knex.connect.raw(`
      CASE
        WHEN tempahanDetails.tempahanDetailsBhgnType = 'Ekor' AND COUNT(*) OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId) > 1 THEN CONCAT('Penama ', ROW_NUMBER() OVER (PARTITION BY tempahanDetailsLiveStok.tempahanDetailsId ORDER BY tempahan.tempahanCreatedDate))
        ELSE '-'
      END AS isUnderSameTempahanId
    `)
    )
    .join("tempahan", "tempahanDetails.tempahanId", "tempahan.tempahanId")
    .join("userWakil", "tempahan.userWakilId", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "user.UserId")
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "wakilPeserta.wakilPesertaId"
    )
    .where("tempahanDetails.implementerId", implementerId)
    .where("tempahan.tempahanStatus", status);

  jumlahTransaksi = await knex
    .connect(`tempahanDetails`)
    .count(`tempahanDetailsId`, { as: "jumlahTransaksi" })
    .where(`implementerId`, implementerId)
    .where(`tempahanDetailsStatus`, status);

  for (let i = 0; i < resultTempahanDetails.length; i++) {
    resultTempahanDetails[i].tempahanCreatedDate = moment(
      resultTempahanDetails[i].tempahanCreatedDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  console.log(implementer);
  console.log(resultTempahanDetails);
  console.log(jumlahTransaksi);

  return [implementer, resultTempahanDetails, jumlahTransaksi];
}

async function getButiranTempahanImplementer(tempahanDetailsId) {
  let tempahanDetails = null;
  let tempahan = null;
  let resit = null;
  let userWakil = null;
  let wakilPeserta = null;
  let liveStok = null;
  let liveStokBahagian = null;

  tempahanDetails = await knex
    .connect(`tempahanDetails`)
    .where(`tempahanDetailsId`, tempahanDetailsId);

  tempahan = await knex
    .connect(`tempahan`)
    .where(`tempahanId`, tempahanDetails[0].tempahanId);

  resit = await knex
    .connect(`resit`)
    .where(`tempahanId`, tempahan[0].tempahanId);

  userWakil = await knex
    .connect(`userWakil`)
    .join(`user`, `userWakil.userId`, `user.userId`)
    .where(`userWakilId`, tempahan[0].userWakilId);

  wakilPeserta = await knex
    .connect(`wakilPeserta`)
    .where(`wakilPesertaId`, tempahanDetails[0].wakilPesertaId);

  liveStok = await knex
    .connect(`tempahanDetailsLiveStok`)
    .join(
      `liveStok`,
      `tempahanDetailsLiveStok.liveStokId`,
      `liveStok.liveStokId`
    )
    .where(`tempahanDetailsLiveStok.tempahanDetailsId`, tempahanDetailsId);

  liveStokBahagian = await knex
    .connect(`tempahanDetailsLiveStok`)
    .join(
      `liveStokBahagian`,
      `tempahanDetailsLiveStok.liveStokBahagianId`,
      `liveStokBahagian.liveStokBahagianId`
    )
    .where(`tempahanDetailsLiveStok.tempahanDetailsId`, tempahanDetailsId);

  return [
    tempahanDetails,
    tempahan,
    resit,
    userWakil,
    wakilPeserta,
    liveStok,
    liveStokBahagian,
  ];
}

async function updateButiranTempahanImplementer(
  wakilPesertaId,
  wakilPesertaName
) {
  let result = null;
  try {
    let datetime = getDateTime();

    let sql = await knex
      .connect(`wakilPeserta`)
      .update({
        wakilPesertaName: wakilPesertaName,
      })
      .where(`wakilPesertaId`, wakilPesertaId);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateTempahanAdmin(
  tempahanId,
  tempahanTarikhBayaran,
  tempahanStatus,
  tempahanTarikhUploadResit
) {
  let result = null;
  try {
    let datetime = getDateTime();

    let sql = await knex
      .connect(`tempahan`)
      .update({
        tempahanTarikhBayaran: tempahanTarikhBayaran,
        tempahanStatus: tempahanStatus,
        tempahanTarikhUploadResit: tempahanTarikhUploadResit,
        tempahanTarikhPengesahan: datetime,
      })
      .where(`tempahanId`, tempahanId);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertResitAdmin(
  tempahanId,
  resitPaymentMethod,
  resitRefNo,
  resitPaymentStatus
) {
  let result = null;
  try {
    let datetime = getDateTime();

    let sql = await knex
      .connect(`resit`)
      .update({
        tempahanTarikhBayaran: tempahanTarikhBayaran,
        resitPaymentMethod: resitPaymentMethod,
        resitRefNo: resitRefNo,
        resitPaymentStatus: resitPaymentStatus,
      })
      .where(`tempahanId`, tempahanId);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getResit(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .join("resit", "tempahan.tempahanId", "=", "resit.tempahanId")
    .join("userWakil", "resit.userWakilId", "=", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "=", "user.userId")
    .where({
      "tempahan.tempahanId": tempahanId,
    })
    .select(
      "resit.*",
      "tempahan.tempahanNo",
      "tempahan.tempahanId",
      "tempahan.tempahanIbadahType",
      "user.userPhoneNo",
      "user.userFullname"
    );

  return result;
}

async function getTempahanDetailsResit(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .join(
      "tempahanDetails",
      "tempahan.tempahanId",
      "=",
      "tempahanDetails.tempahanId"
    )
    .leftJoin(
      "tempahanDetailsLiveStok",
      "tempahanDetails.tempahanDetailsId",
      "=",
      "tempahanDetailsLiveStok.tempahanDetailsId"
    )
    .leftJoin(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "=",
      "liveStok.liveStokId"
    )
    .leftJoin(
      "implementer",
      "tempahanDetails.implementerId",
      "=",
      "implementer.implementerId"
    )
    .join(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "=",
      "wakilPeserta.wakilPesertaId"
    )
    .where({
      "tempahan.tempahanId": tempahanId,
    })
    .distinct("wakilPeserta.wakilPesertaName")
    .select(
      "liveStok.liveStokNo",
      "tempahan.userWakilId",
      "tempahan.tempahanIbadahType",
      "tempahan.tempahanStatus",
      "tempahanDetails.tempahandetailsId",
      "tempahanDetails.tempahanDetailsPrice",
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnType",
      "tempahanDetails.implementerLokasiId",
      "tempahanDetails.tempahanDetailsBhgnCount",
      "tempahanDetails.tempahanDetailsBhgnType",
      "tempahanDetails.tempahanDetailsStatus",
      "tempahanDetails.implementerLokasiId",
      "implementer.implementerNegara",
      "implementer.implementerKawasan",
      "liveStok.liveStokId"
    );

  return result;
}

// GET TEMPAHAN'S CALLBACK REASON
async function getCallback(order_id) {
  let result = null;

  result = await knex
    .connect("callback")
    .join("tempahan", "callback.order_id", "=", "tempahan.tempahanNo")
    .where({
      "callback.order_id": order_id,
    })
    .select("callback.reason")
    .orderBy("id", "desc")
    .limit(1);

  return result;
}

// GET TEMPAHAN REFUND DETAILS
async function getRefund(tempahanId) {
  let result = null;

  result = await knex
    .connect("refund")
    .join("tempahan", "refund.tempahanId", "=", "tempahan.tempahanId")
    .where({
      "refund.tempahanId": tempahanId,
    })
    .select("refund.*");

  return result;
}

async function getUserWakilDetails(tempahanNo) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .join("userWakil", "tempahan.userWakilId", "=", "userWakil.userWakilId")
    .join("user", "userWakil.userId", "=", "user.userId")
    .where({
      "tempahan.tempahanNo": tempahanNo,
    })
    .select(
      "tempahan.userWakilId",
      "user.userFullname",
      "user.userEmail",
      "user.userPhoneNo"
    );

  return result;
}

// SEND SMS
async function sendSMS(body, phones) {
  let result = null;

  try {
    await axios
      .post(
        "https://manage.smsniaga.com/api/send",
        {
          body: body,
          phones: [phones],
          sender_id: "YAYASAN IKHLAS",
        },
        {
          headers: {
            Authorization: `Bearer ` + process.env.TOKEN,
          },
        }
      )
      .then(function (response) {
        result = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GET LIVESTOK NO & PESERTA NAMES
async function getLivestokAndPeserta(implementerId, tempahanStatus) {
  let result = null;

  /**
   * SELECT
    tempahanDetailsLiveStok.liveStokId,
    liveStok.liveStokNo,
    tempahanDetailsLiveStok.wakilPesertaId,
    wakilPeserta.wakilPesertaName,
    tempahanDetailsLiveStok.tempahanId,
    liveStok.liveStokAddress
FROM
    tempahan 
    JOIN tempahanDetailsLiveStok ON tempahan.tempahanId = tempahanDetailsLiveStok.tempahanId
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
WHERE
    tempahanDetailsLiveStok.implementerId = 1
    AND tempahan.tempahanStatus = 'Success'
ORDER BY tempahanDetailsLiveStok.liveStokId ASC
   */

  result = await knex.connect.raw(
    `SELECT
    tempahanDetailsLiveStok.liveStokId,
    liveStok.liveStokNo,
    tempahanDetailsLiveStok.wakilPesertaId,
    wakilPeserta.wakilPesertaName,
    tempahanDetailsLiveStok.tempahanId,
    liveStok.liveStokAddress
FROM
    tempahan 
    JOIN tempahanDetailsLiveStok ON tempahan.tempahanId = tempahanDetailsLiveStok.tempahanId
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
WHERE
    tempahanDetailsLiveStok.implementerId =  ` +
      implementerId +
      `
    AND tempahan.tempahanStatus = '` +
      tempahanStatus +
      `'
ORDER BY tempahanDetailsLiveStok.liveStokId ASC`
  );

  return result;
}

// GET SIJIL FOR PESERTA
async function getSijilPerserta(tempahanDetailsId) {
  let result = null;

  try {
    result = await knex
      .connect(`tempahanDetailsLiveStok`)
      .where({
        tempahanDetailsId: tempahanDetailsId,
      })
      .distinct("sijilPath", "sijilStatus");
  } catch (error) {
    console.log(error);
  }

  return result[0];
}

// GET TIMELINE FOR TEMPAHAN
async function getTempahanTimeline(tempahanDetailsId) {
  let result = null;

  try {
    /**
     * SELECT
    aktivitiDateLivestok.tempahanDetailsId,
    aktivitiDate.aktivitiDateTarikhPelaksanaan,
    aktivitiLookup.aktivitiLookupTajuk
FROM
    aktivitiDate
    JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
WHERE
    aktivitiDateLivestok.tempahanDetailsId = 613
     */

    result = await knex
      .connect(`aktivitiDate`)
      .join(
        "aktivitiLookup",
        "aktivitiDate.aktivitiLookupId",
        "=",
        "aktivitiLookup.aktivitiLookupId"
      )
      .join(
        "aktivitiDateLivestok",
        "aktivitiDate.aktivitiDateId",
        "=",
        "aktivitiDateLivestok.aktivitiDateId"
      )
      .where({
        "aktivitiDateLivestok.tempahanDetailsId": tempahanDetailsId,
      })
      .select(
        "aktivitiDateLivestok.tempahanDetailsId",
        "aktivitiDate.aktivitiDateTarikhPelaksanaan as aktivitiDateTarikhPelaksanaan1",
        "aktivitiDate.aktivitiDateTarikhPelaksanaan as aktivitiDateTarikhPelaksanaan2",
        "aktivitiDate.aktivitiDateTarikhPelaksanaan as aktivitiDateTarikhPelaksanaan3",
        "aktivitiLookup.aktivitiLookupTajuk"
      );
  } catch (error) {
    console.log(error);
  }

  return result;
}

module.exports = {
  getTempahanAdmin,
  getTempahanAdminDateToFrom,
  getTempahanAdminDateToFromPending,
  getTempahanAdminByStatus,
  getTempahanAdminSuccess,
  getTempahanAdminFailed,
  getTempahanAdminPendingOrder,
  getTempahanAdminPendingPayment,
  getTempahanAdminCancelOrder,
  getTempahanEjen,
  getTempahanEjenDateToFrom,
  getTempahanEjenAsc,
  getTempahanEjenSuccess,
  getTempahanEjenFailed,
  getTempahanEjenPendingOrder,
  getTempahanEjenPendingPayment,
  getTempahanEjenCancelOrder,
  getTempahanEjenByStatus,
  getTempahanAdminByDateAsc,
  getButiranTempahanAdmin,
  getButiranTempahanEjen,
  getTempahanDetailsLiveStok,
  getTempahanImplementer,
  getTempahanImplementerAsc,
  getTempahanImplementerSuccess,
  getTempahanImplementerFailed,
  getTempahanImplementerPendingOrder,
  getTempahanImplementerPendingPayment,
  getTempahanImplementerCancelOrder,
  getTempahanImplementerByStatus,
  getButiranTempahanImplementer,
  getResit,
  getTempahanDetailsResit,
  getCallback,
  getRefund,
  getUserWakilDetails,
  getLivestokAndPeserta,
  getSijilPerserta,
  getTempahanTimeline,
  getTempahanAdminPendingPaymentAsc,
  getTempahanAdminByStatusDate,

  updateButiranTempahanImplementer,
  updateTempahanAdmin,

  insertResitAdmin,

  sendSMS,
};
