const knex = require("../../connection");


async function getUserWakilId(userFullname,userPhoneNo,userEmail) {

  let result = null;

  result = await knex.connect('user')
  .join('userWakil', 'user.userId', '=', 'userWakil.userId')
  .where({
      'user.userFullname': userFullname,
      'user.userPhoneNo': userPhoneNo,
      'user.userEmail': userEmail,
  })
  .select('userWakil.userWakilId')

  console.log(result);

  return result;
}

module.exports = {
    getUserWakilId,
};