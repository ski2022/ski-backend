const knex = require("../connection.js");
const moment = require("moment");
const sha = require("sha256");

async function login(username, password) {
  let result = null;
  try {
    let user = await knex
      .connect(`user`)
      .leftJoin("userEjen", "userEjen.userId", "user.userId")
      .where(`userUsername`, username)
      .andWhere(`userPassword`, sha(password));
    console.log(`user sql: `, user);
    if (user.length > 0) {
      result = user[0];
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }
  // console.log(`user sql: `, result);

  return result;
}

module.exports = {
  login,
};
