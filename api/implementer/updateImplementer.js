const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementer/implementer.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let username = null;
  let password = null;
  let fullname = null;
  let email = null;
  let phoneno = null;
  let kawasan = null;
  let negara = null;
  let ibadah = null;
  let implementerStatus = null;

  let alamat = null;
  let postcode = null;
  let bandar = null;
  let negeri = null;
  let noakaun = null;
  let namapemegangbank = null;
  let namabank = null;
  let bankcode = null;
  let closedate = null;
  //   let haiwan = null;

  let jenis = null;
  let hargaekor = null;
  let hargabhg = null;
  let statusHaiwanKorban = null;
  let implementerId = null;
  let implementerLokasiId = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    implementerId = param.implementerId;
    implementerLokasiId = param.implementerLokasiId;
    username = param.username;
    password = param.password;
    fullname = param.fullname;
    email = param.email;
    phoneno = param.phoneno;
    kawasan = param.kawasan;
    negara = param.negara;
    ibadah = param.ibadah;
    implementerStatus = param.implementerStatus;

    alamat = param.alamat;
    postcode = param.postcode;
    bandar = param.bandar;
    negeri = param.negeri;
    noakaun = param.noakaun;
    namapemegangbank = param.namapemegangbank;
    namabank = param.namabank;
    bankcode = param.bankcode;
    closedate = param.closedate;

    jenis = param.jenis;
    hargaekor = param.hargaekor;
    hargabhg = param.hargabhg;
    statusHaiwanKorban = param.statusHaiwanKorban;

    //VALIDATE USERNAME & EMAIL

    let checkUsername = await model.checkUsername(implementerId, username);
    let checkEmail = await model.checkEmail(implementerId, email);
    let checkPhone = await model.checkPhone(implementerId, phoneno);
    let message = null;

    // let checkUsernameEmail = await model.checkUsernameEmail(username, email, phoneno)
    //     if (checkUsernameEmail == false) {
    //       result = {
    //         status: "gagal",
    //         message: 'Username, Email atau Nombor Telefon telah wujud.'
    //       }
    //     }

    if (checkUsername == false || checkEmail == false || checkPhone == false) {
      console.log("Masuk banyak");
      if (checkUsername == false) {
        message = "Username telah wujud.";
        result = {
          status: "gagal",
          message: message,
        };
      } else {
        console.log("Update Username");
      }
      if (checkEmail == false) {
        console.log("Email wujud");
        message += " | Email telah wujud.";
        result = {
          status: "gagal",
          message: message,
        };
      } else {
        console.log("Update Email");
      }
      if (checkPhone == false) {
        console.log("Phone wujud");
        message += " | Phone telah wujud.";
        result = {
          status: "gagal",
          message: message,
        };
      } else {
        console.log("Update Phone");
      }
    } else {
      // GET USER FUNCTION
      let updateImplementer = await model.updateImplementer(
        implementerId,
        username,
        fullname,
        email,
        password,
        phoneno,
        negeri,
        negara,
        ibadah,
        implementerStatus
      );
      let updateImplementerLokasi = await model.updateImplementerLokasi(
        implementerId,
        implementerLokasiId,
        alamat,
        postcode,
        bandar,
        negeri,
        noakaun,
        namapemegangbank,
        namabank,
        bankcode,
        closedate
      );
      let updateImplementerLokasiHaiwan =
        await model.updateImplementerLokasiHaiwan(
          implementerId,
          implementerLokasiId,
          jenis,
          hargaekor,
          hargabhg,
          statusHaiwanKorban
        );
      let updatesetupKorbanImplementerLokasi =
        await model.updatesetupKorbanImplementerLokasi(
          implementerId,
          updateImplementerLokasi
        );
      if (updateImplementer != false) {
        result = {
          message: "anda berjaya Update Implementer",
          environment: process.env.ENVIRONMENT,
          userdata: updateImplementer,
          test: updateImplementer,
          jenis: jenis.length,
          hargaekor: hargaekor.length,
          hargabhg: hargabhg.length,
          statusHaiwanKorban: statusHaiwanKorban.length,
        };
      } else {
        result = {
          message: "User Update fail",
        };
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
  // try {
  //     param = req.body;

  //     username = param.username;
  //     password = param.password;
  //     fullname = param.fullname;
  //     email = param.email;
  //     phoneno = param.phoneno;
  //     kawasan = param.kawasan;
  //     negara = param.negara;
  //     ibadah = param.ibadah;
  //     haiwan = param.haiwan;

  //     // const obj = [{name: "John", age: 30, city: "New York"},
  //     // {name: "Ismat", age: 22, city: "Kuala Lumpur"},
  //     // {name: "Iqmal", age: 22, city: "Kuala Lumpur"},
  //     // {name: "Julap", age: 22, city: "Kuala Lumpur"}];

  //     for (let i = 0; i < haiwan.length; i++) {
  //     const [ user ] = await knex.connect
  //       .insert({
  //         username: username
  //       })
  //       .into('implementer')
  //       .returning('implementerId')
  //       .then(([ userId ]) => knex.connect
  //         .insert({ implementerLokasiHaiwanJenis: haiwan[i], implementerId:userId})
  //         .into('implementerLokasiHaiwan')
  //       )
  //     }

  //     result = haiwan;

  //     status = 'berjaya';
  //     message = 'masuk dah dalam db';

  //   } catch (error) {
  //     status = "unsuccess";
  //     message = "API error. " + error;
  //   }
  //   res.status(200).json(result);
});

module.exports = router;
// function functionResponse(status, message) {
//     return {
//       statusCode: 200,
//       headers: {
//         "Access-Control-Allow-Origin": "*",
//       },
//       body: JSON.stringify({
//         status_code: 200,
//         status: status,
//         message: message,
//         // data: data,
//       }),
//     };
// }
