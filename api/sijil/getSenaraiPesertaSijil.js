const express = require("express");
const router = express.Router();
const model = require("../../function/sijil/sijil");

router.get("/getSenaraiPesertaSijil", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  try {
    param = req.query;
    implementerId = param.implementerId;

      let getSenaraiPesertaSijil = await model.getSenaraiPesertaSijil(implementerId);

      if (getSenaraiPesertaSijil[0] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai peserta sijil",
          data: getSenaraiPesertaSijil[0],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai peserta sijil',
          data: {},
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.get("/getSenaraiPesertaSijilActive", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  try {
    param = req.query;
    implementerId = param.implementerId;

      let getSenaraiPesertaSijilActive = await model.getSenaraiPesertaSijilActive(implementerId);

      if (getSenaraiPesertaSijilActive[0] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai peserta sijil ACTIVE",
          data: getSenaraiPesertaSijilActive[0],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai peserta sijil ACTIVE',
          data: {},
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;