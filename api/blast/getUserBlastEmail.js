const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/blast/blast.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");
// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/",  async (req, res) => {
  let param = null;
  let result = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    // implementerStatusCode = param.implementerStatusCode;

    // GET USER FUNCTION
    const getUserBlastEmail = await model.getUserBlastEmail();

    if (getUserBlastEmail[0][0]) {
      result = {
        status: "berjaya",
        message: "anda berjaya dapatkan senarai User Blast Email",
        environment: process.env.ENVIRONMENT,
        userdata: getUserBlastEmail[0][0],
      };
    } else {
      result = {
          status: "gagal",
          message: 'User Type does not exist'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
