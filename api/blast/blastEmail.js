const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/blast/blast.js"); // INCLUDE FUNCTION FILE\
const sgMail = require('@sendgrid/mail')
// const client = require('@sendgrid/mail');
// const nodemailer = require("nodemailer"); 

router.post("/",  async (req, res) => {
    let param = null;
    let result = null;

    let EmailAPerson = null;
    let SubjectAPerson = null;
    let TextAPerson = null;
    let resultEmail = null

    try{

      param = req.body;

      EmailAPerson = param.to;
      SubjectAPerson = param.subject;
      TextAPerson = param.text;


      sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msg = {
      to: EmailAPerson, // Change to your recipient
      from: {
        email: 'marketing.ikhlas@gmail.com',
        name: 'Yayasan Ikhlas'}, 
      // 'marketing.ikhlas@gmail.com',
       // Change to your verified sender
      subject: SubjectAPerson,
      // text: TextAPerson,
      html: TextAPerson
      // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    }
    await sgMail
    .send(msg)
    .then(function (response) {
        resultEmail = response[0].statusCode
    })
    .catch(function (error) {
        console.error(error)
    })
    if (resultEmail == 202) {
      let insertNotificationBlastEmail = await model.insertNotificationBlastEmail(
        1,
        EmailAPerson,
        SubjectAPerson,
        TextAPerson,
        'Email',
        resultEmail
      ) 
      result = {
        data: insertNotificationBlastEmail[1]
      }
    }

    }catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }
  
  
    // VARIABLE SETUP
  
    // const SENDGRID_API_KEY = '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';
    
 
//METHOD 1
//     sgMail.setApiKey='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'
//     const msg = {
//     to: 'b031920017@student.utem.edu.my', // Change to your recipient
//     from: 'support@toyyibpay.com', // Change to your verified sender
//     subject: 'Testing API SKI',
//     text: 'Testing API SKI',
//     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    
//     }
    
// sgMail
//   .send(msg)
//   .then(() => {
//     console.log('Email sent')
//     console.log('lalu sini')
//   })
//   .catch((error) => {
//     console.error(error)
//     console.log('errorlah')
//     console.log(sgMail.setApiKey)
//   })

//METHOD 1 V3

// client.setApiKey('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');

// const message = {
//   personalizations: [
//     {
//       from: {
//         email: 'supportv3@dev-toyyibpay.com',
//         name: 'Toyyibpay Support'
//       },
//       to: [
//         {
//           email: 'akmalkhairi24@gmail.com',
//           name: 'Akmal Khairi'
//         }
//       ],
//     }
//   ],
//   subject: 'Your Example Order Confirmation',
//   content: [
//     {
//       type: 'text/html',
//       value: '<p>Hello from Twilio SendGrid!</p><p>Sending with the email service trusted by developers and marketers for <strong>time-savings</strong>, <strong>scalability</strong>, and <strong>delivery expertise</strong>.</p><p>%open-track%</p>'
//     }
//   ],
//   sendAt: 1617260400,
//   batchId: 'AsdFgHjklQweRTYuIopzXcVBNm0aSDfGHjklmZcVbNMqWert1znmOP2asDFjkl',
//   asm: {
//     groupId: 12345,
//     groupsToDisplay: [
//       12345
//     ]
//   },
//   ipPoolName: 'transactional email',
//   mailSettings: {
//     bypassListManagement: {
//       enable: false
//     },
//     footer: {
//       enable: false
//     },
//     sandboxMode: {
//       enable: false
//     }
//   },
//   trackingSettings: {
//     clickTracking: {
//       enable: true,
//       enableText: false
//     },
//     openTracking: {
//       enable: true,
//       substitutionTag: '%open-track%'
//     },
//     subscriptionTracking: {
//       enable: false
//     }
//   }
// };

// client
//   .send(message)
//   .then(() => console.log('Mail sent successfully'))
//   .catch(error => {
//     console.error(error);
//   });


//METHOD 2
// let transporter = nodemailer.createTransport({
//     sendmail: true,
//     newline: 'unix',
//     path: '/usr/sbin/sendmail'
// });

// console.log("Process Started To Send An Email");     

// transporter.sendMail({
//     from: 'admin@toyyibpay.com',
//     to: 'akmalkhairi24@gmail.com',
//     subject: 'Message',
//     text: 'I hope this message gets delivered!'
// }, (err, info) => {
//     // console.log(info.envelope);
//     // console.log(info.messageId);
// });

//METHOD
// const transporter = nodemailer.createTransport({
//         sendmail: true,
//         newline: 'unix',
//         path: '/usr/sbin/sendmail'
//     });

// const mailOptions = {
//         from: 'admin@toyyibpay.com',
//         to: 'akmalkhairi24@gmail.com',
//         subject: 'Sending Email using Node.js',
//         text: 'That was easy!'
//       };

// transporter.sendMail(mailOptions, function(error, info){
//     if (error) {
//         console.log(error);
//     } else {
//         console.log('Email sent: ' + info.response);
//     }
// });

//METHOD 3
// async function main() {  

    // let transporter = nodemailer.createTransport({         
    // host: "127.0.0.1",        
    // port: 25,        
    // secure: false,        
    // tls: {             
    // rejectUnauthorized:false
    //         }    
    // });    
    
    // for(i=0;i<1;i++)  
          
    // await transporter.sendMail({            
    // from: "admin@toyyibpay.com",           
    //  to: "b031920017@student.utem.edu.my",            
    // subject: "Hello"+i,            
    //   html: "<b>Helloworld!"+i+"</b>",       
    //  });     

    // console.log("Message sent");

    // }
    // main().catch(console.error);
    // RETURN
    res.status(200).json(result);
  });

module.exports = router;