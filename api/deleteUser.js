const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../function/user.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// DELETE USER
router.delete("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let username = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    username = param.username;

    // GET USER FUNCTION
    const deleteUser = await model.deleteUser(username);

    if (deleteUser != false) {
      result = {
        message: "anda berjaya membuang pengguna",
        environment: process.env.ENVIRONMENT,
        userdata: deleteUser,
        test: deleteUser,
      };
    } else {
      result = {
          message: 'User Deletion fail'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
