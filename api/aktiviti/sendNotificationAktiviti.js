const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");


router.post("/", async (req, res) => {
  let param = null;
  let result = null;
  let modalNamaWakil = null, modalTelefonWakil = null, 
  WhatsAppText = null, SMSText = null, SN = null, aktivitiDateLivestokId = null,
  SMSChecker = null, WSChecker = null, EmailChecker = null

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body
    modalEmelWakil = param.EmelWakil
    modalNamaWakil = param.NamaWakil
    modalTelefonWakil = param.TelefonWakil
    WhatsAppText = param.WhatsAppText
    SMSText = param.SMSText
    EmailSubject = param.EmailSubject
    EmailText = param.EmailText
    SN = param.HaiwanSN
    aktivitiDateLivestokId = param.Id

    SMSChecker = param.SMSChecker
    WSChecker = param.WSChecker
    EmailChecker = param.EmailChecker

      console.log('--- sendNotificationAktiviti PROCESS START ---')

      let sendNotificationAktiviti = await model.sendNotificationAktiviti(
        modalEmelWakil,
        modalNamaWakil,
        modalTelefonWakil,
        WhatsAppText,
        SMSText,
        EmailSubject,
        EmailText,
        SN,
        SMSChecker,
        WSChecker,
        EmailChecker
      )
      
      if (WSChecker === 1) {
        let insertNotificationAktivitiWS = await model.insertNotificationAktiviti(
            1,
            modalTelefonWakil,
            null,
            WhatsAppText,
            'whatsApp',
            aktivitiDateLivestokId,
            sendNotificationAktiviti.resultWhatsapp
        )
      }

      if (SMSChecker === 1) {
        let insertNotificationAktivitiSMS = await model.insertNotificationAktiviti(
            1,
            modalTelefonWakil,
            null,
            SMSText,
            'SMS',
            aktivitiDateLivestokId,
            sendNotificationAktiviti.resultSMS
        )
      }

      if (EmailChecker === 1) {
        let insertNotificationAktivitiEmail = await model.insertNotificationAktiviti(
            1,
            modalEmelWakil,
            EmailSubject,
            EmailText,
            'Email',
            aktivitiDateLivestokId,
            sendNotificationAktiviti.resultEmail
        )
      }

      if (sendNotificationAktiviti != false) {
        result = {
          status: "berjaya",
          message: "ANDA BERJAYA SEND NOTI!",
          data: sendNotificationAktiviti
        };
      } else {
        result = {
          status: "gagal",
          message: 'ANDA TIDAK BERJAYA SEND NOTI!',
          data: null,
          data2: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;