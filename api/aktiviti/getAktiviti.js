const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    implementerId = param.implementerId;

      let aktiviti = await model.getAktiviti(implementerId);

      if (aktiviti[0] != false || aktiviti[1] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai aktiviti",
          data: aktiviti[0],
          name: aktiviti[1],
          data2: aktiviti[2],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai aktiviti'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;