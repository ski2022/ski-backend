const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  console.log(req.query)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    implementerId = param.implementerId;
    notificationRefId = param.notificationRefId

      console.log('--- getNotificationLog ---')
      let getData = await model.getNotificationLog(implementerId, notificationRefId);

      if (getData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan data",
          data: getData[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan data',
          data: null,
          data2: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;