const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../function/login.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let username = null;
  let password = null;

  // VARIABLE SETUP
  // let userType = null;

  try {
    param = req.query;

    username = param.username;
    password = param.password;

    // GET USER FUNCTION
    const getUser = await model.login(username, password);
    console.log("PARAM", param);
    console.log(`credential: `, username, password);
    console.log(`get user: `, getUser);
    if (getUser != false) {
      result = {
        auth: true,
        // accessToken: accessToken,
        userid: getUser.userId,
        username: getUser.userUsername,
        password: getUser.userPassword,
        userType: getUser.userType,
        fullname: getUser.userFullname,
      };
    } else {
      result = {
        auth: false,
        message: "User does not exist",
        env: process.env.ENVIRONMENT,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
