const express = require("express");
const router = express.Router();

// REROUTE

// CONFIGURATION
router.use("/getTahunKorban", require("./configuration/getTahunKorban"));
router.use("/getTempatKorban", require("./configuration/getTempatKorban"));
router.use("/getListKawasan", require("./configuration/getListKawasan"));
router.use("/getHargaHaiwan", require("./configuration/getHargaHaiwan"));
router.use("/getKomisyenEjen", require("./configuration/getKomisyenEjen"));
router.use(
  "/insertTempatKorban",
  require("./configuration/insertTempatKorban")
);
router.use(
  "/insertTahunKorban",
  require("./configuration/insertConfigurationSetupKorban")
);

// AUTH
router.use("/auth", require("./auth"));

// USER MANAGEMENT (GET, INSERT, UPDATE) *EXAMPLE
router.use("/user", require("./user")); // WILL GO TO api/user.js

// LOGIN
router.use("/login", require("./login"));
router.use("/validate", require("./validate"));

// VIEW  ALL USER
router.use("/viewUser", require("./viewUser"));

// INSERT USER
router.use("/insertUser", require("./insertUser"));

// DELETE USER
router.use("/deleteUser", require("./deleteUser"));

// DELETE USER
router.use("/updateUser", require("./updateUser"));

router.use("/insertEjen", require("./ejen/insertEjen"));
router.use("/getEjen", require("./ejen/getEjen"));
router.use("/getEjenTransaksi", require("./ejen/getEjenTransaksi"));
router.use("/getEjenBahagian", require("./ejen/getEjenBahagian"));
router.use("/getEjenSortByDateAsc", require("./ejen/getEjenSortByDateAsc"));
router.use(
  "/getEjenSortByDateAscTransaksi",
  require("./ejen/getEjenSortByDateAscTransaksi")
);
router.use(
  "/getEjenSortByDateAscBahagian",
  require("./ejen/getEjenSortByDateAscBahagian")
);
router.use("/getEjenSortByDateDesc", require("./ejen/getEjenSortByDateDesc"));
router.use(
  "/getEjenSortByDateDescTransaksi",
  require("./ejen/getEjenSortByDateDescTransaksi")
);
router.use(
  "/getEjenSortByDateDescBahagian",
  require("./ejen/getEjenSortByDateDescBahagian")
);
router.use(
  "/getEjenSortByCommisionDesc",
  require("./ejen/getEjenSortByCommisionDesc")
);
// router.use('/getEjen', require("./getEjen"));
router.use("/getButiranEjen", require("./ejen/getButiranEjen"));
router.use("/updateEjen", require("./ejen/updateEjen"));

router.use("/getEjenSettlement", require("./ejen/getEjenSettlement"));

// Get ejen cawangan under ejen duta
router.use("/getEjenCawangan", require("./ejen/getEjenCawangan.js"));

router.use("/getConfiguration", require("./configuration/getConfiguration"));

router.use(
  "/getImplementerLokasiHaiwan",
  require("./implementerLokasiHaiwan/getImplementerLokasiHaiwan")
);

router.use("/insertLiveStok", require("./liveStok/insertLiveStok"));

router.use("/getNegara", require("./configuration/getNegara"));
router.use(
  "/getNegaraImplementer",
  require("./configuration/getNegaraImplementer")
);

router.use("/getLiveStok", require("./liveStok/getLiveStok"));
router.use("/getLiveStokInActive", require("./liveStok/getLiveStokInActive"));

router.use("/getLiveStokByStatus", require("./liveStok/getLiveStokByStatus"));
router.use("/getLiveStokActive", require("./liveStok/getLiveStokActive"));

router.use("/getLiveStokBahagian", require("./liveStok/getLiveStokBahagian"));
router.use("/getListPengguna", require("./configuration/getListPengguna"));
router.use("/getListJenisEjen", require("./configuration/getListJenisEjen"));
router.use("/getListDutaRef", require("./configuration/getListDutaRef"));
router.use("/getAktivitiLookup", require("./aktiviti/getAktivitiLookup"));
router.use("/getAktiviti", require("./aktiviti/getAktiviti"));
router.use(
  "/insertSetupKorbanAktiviti",
  require("./aktiviti/insertSetupKorbanAktiviti")
);
router.use("/getImplementerById", require("./aktiviti/getImplementerById"));
router.use("/getAktivitiDetails", require("./aktiviti/getAktivitiDetails"));
router.use(
  "/getTarikhAktiviti",
  require("./aktiviti/getSenaraiTarikhAktiviti")
);

router.use("/getTempahanAdmin", require("./tempahan/getTempahanAdmin"));
router.use(
  "/getTempahanAdminDateToFrom",
  require("./tempahan/getTempahanAdminDateToFrom")
);
router.use(
  "/getTempahanAdminDateToFromPending",
  require("./tempahan/getTempahanAdminDateToFromPending")
);
router.use(
  "/getTempahanAdminByDateAsc",
  require("./tempahan/getTempahanAdminByDateAsc")
);
router.use(
  "/getTempahanAdminSuccess",
  require("./tempahan/getTempahanAdminSuccess")
);
router.use(
  "/getTempahanAdminFailed",
  require("./tempahan/getTempahanAdminFailed")
);
router.use(
  "/getTempahanAdminPendingOrder",
  require("./tempahan/getTempahanAdminPendingOrder")
);
router.use(
  "/getTempahanAdminPendingPayment",
  require("./tempahan/getTempahanAdminPendingPayment")
);
router.use(
  "/getTempahanAdminCancelOrder",
  require("./tempahan/getTempahanAdminCancelOrder")
);
router.use("/getTempahanEjen", require("./tempahan/getTempahanEjen"));
router.use(
  "/getTempahanEjenDateToFrom",
  require("./tempahan/getTempahanEjenDateToFrom")
);
router.use("/getTempahanEjenAsc", require("./tempahan/getTempahanEjenAsc"));
router.use(
  "/getTempahanEjenSuccess",
  require("./tempahan/getTempahanEjenSuccess")
);
router.use(
  "/getTempahanEjenFailed",
  require("./tempahan/getTempahanEjenFailed")
);
router.use(
  "/getTempahanEjenPendingOrder",
  require("./tempahan/getTempahanEjenPendingOrder")
);
router.use(
  "/getTempahanEjenPendingPayment",
  require("./tempahan/getTempahanEjenPendingPayment")
);
router.use(
  "/getTempahanEjenCancelOrder",
  require("./tempahan/getTempahanEjenCancelOrder")
);
router.use(
  "/getButiranTempahanAdmin",
  require("./tempahan/getButiranTempahanAdmin")
);
router.use(
  "/getButiranTempahanEjen",
  require("./tempahan/getButiranTempahanEjen")
);
router.use(
  "/getTempahanDetailsLiveStok",
  require("./tempahan/getTempahanDetailsLiveStok")
);
router.use(
  "/getTempahanImplementer",
  require("./tempahan/getTempahanImplementer")
);
router.use(
  "/getTempahanImplementerAsc",
  require("./tempahan/getTempahanImplementerAsc")
);
router.use(
  "/getTempahanImplementerSuccess",
  require("./tempahan/getTempahanImplementerSuccess")
);
router.use(
  "/getTempahanImplementerFailed",
  require("./tempahan/getTempahanImplementerFailed")
);
router.use(
  "/getTempahanImplementerPendingOrder",
  require("./tempahan/getTempahanImplementerPendingOrder")
);
router.use(
  "/getTempahanImplementerPendingPayment",
  require("./tempahan/getTempahanImplementerPendingPayment")
);
router.use(
  "/getTempahanImplementerCancelOrder",
  require("./tempahan/getTempahanImplementerCancelOrder")
);

router.use(
  "/getTempahanImplementerPdf",
  require("./tempahan/getTempahanImplementerPdf")
);
router.use("/getTempahanSijil", require("./tempahan/getTempahanSijil"));
router.use("/getTempahanTimeline", require("./tempahan/getTempahanTimeline"));

router.use(
  "/getButiranTempahanImplementer",
  require("./tempahan/getButiranTempahanImplementer")
);
router.use("/getHaiwanKorban", require("./haiwanKorban/getHaiwanKorban"));
router.use(
  "/getImplementerHaiwanKorban",
  require("./haiwanKorban/getImplementerHaiwanKorban")
);
router.use(
  "/getButiranHaiwanKorban",
  require("./haiwanKorban/getButiranHaiwanKorban")
);
router.use(
  "/getTempahanAdminByStatus",
  require("./tempahan/getTempahanAdminByStatus")
);
router.use(
  "/getTempahanEjenByStatus",
  require("./tempahan/getTempahanEjenByStatus")
);
router.use(
  "/getTempahanImplementerByStatus",
  require("./tempahan/getTempahanImplementerByStatus")
);
router.use(
  "/updateButiranTempahanImplementer",
  require("./tempahan/updateButiranTempahanImplementer")
);
router.use("/updateTempahanAdmin", require("./tempahan/updateTempahanAdmin"));
router.use("/getStatChartAdmin", require("./dashboard/getStatChartAdmin"));
router.use("/getResitTempahan", require("./tempahan/getResitTempahan"));
router.use("/getOverviewAdmin", require("./dashboard/getOverviewAdmin"));
router.use("/getDonutChartAdmin", require("./dashboard/getDonutChartAdmin"));
router.use(
  "/getLatestTempahanAdmin",
  require("./dashboard/getLatestTempahanAdmin")
);
router.use("/getStatChartEjen", require("./dashboard/getStatChartEjen"));
router.use("/getOverviewEjen", require("./dashboard/getOverviewEjen"));
router.use(
  "/getLatestTempahanEjen",
  require("./dashboard/getLatestTempahanEjen")
);
router.use("/getDonutChartEjen", require("./dashboard/getDonutChartEjen"));
router.use(
  "/getRevenueAreaChartAdmin",
  require("./dashboard/getRevenueAreaChartAdmin")
);
router.use(
  "/getTargetsBarChartAdmin",
  require("./dashboard/getTargetsBarChartAdmin")
);
router.use(
  "/getRevenueAreaChartEjen",
  require("./dashboard/getRevenueAreaChartEjen")
);
router.use(
  "/getTargetsBarChartEjen",
  require("./dashboard/getTargetsBarChartEjen")
);
router.use(
  "/getTempahanAdminPendingPaymentAsc",
  require("./tempahan/getTempahanAdminPendingPaymentAsc")
);
router.use(
  "/getTempahanAdminByStatusDate",
  require("./tempahan/getTempahanAdminByStatusDate")
);

router.use("/insertTraining", require("./training/insertTraining"));
router.use("/updateTraining", require("./training/updateTraining"));

//START IMPLEMENTER SECTION
// INSERTIMPLEMENTER
router.use("/insertImplementer", require("./implementer/insertImplementer"));
// UPDATEIMPLEMENTER
router.use("/updateImplementer", require("./implementer/updateImplementer"));
// DISPLAYIMPLEMENTER
router.use("/displayImplementer", require("./implementer/displayImplementer"));
// DISPLAYIMPLEMENTERBUTIRAN
router.use(
  "/getImplementerButiran",
  require("./implementer/getImplementerButiran")
);
//END IMPLEMENTER SECTION

//START IMPLEMENTERLOKASI SECTION
// INSERTIMPLEMENTERLOKASI
router.use(
  "/insertImplementerLokasi",
  require("./implementerLokasi/insertImplementerLokasi")
);
//END IMPLEMENTERLOKASI SECTION

//START IMPLEMENTERLOKASIHAIWAN SECTION
// INSERT IMPLEMENTER LOKASI HAIWAN
router.use(
  "/insertImplementerLokasiHaiwan",
  require("./implementerLokasiHaiwan/insertImplementerLokasiHaiwan")
);
//END IMPLEMENTERLOKASIHAIWAN SECTION

// BLAST SMS/EMAIL
router.use("/blastEmail", require("./blast/blastEmail"));
router.use("/blastEmailSeveral", require("./blast/blastEmailSeveral"));
router.use("/blastSmsAPerson", require("./blast/blastSmsAPerson"));
router.use("/blastSmsSeveralPerson", require("./blast/blastSmsSeveralPerson"));
router.use("/getUserBlastEmail", require("./blast/getUserBlastEmail"));
router.use("/getEjenBlastEmail", require("./blast/getEjenBlastEmail"));
router.use("/getWakilBlastEmail", require("./blast/getWakilBlastEmail"));
router.use(
  "/getImplementerBlastEmail",
  require("./blast/getImplementerBlastEmail")
);
router.use("/getListBlast", require("./blast/getListBlast"));

// BLAST SMS/EMAIL

//START PENGGUNA SECTION
// INSERT PENGGUNA (WAKIL)
router.use("/insertPengguna", require("./pengguna/insertPengguna"));
// UPDATE PENGGUNA (WAKIL)
router.use("/updatePengguna", require("./pengguna/updatePengguna"));
// DELETE PENGGUNA
router.use("/deletePengguna", require("./pengguna/deletePengguna"));
// DISPLAYPENGGUNA
router.use("/getPengguna", require("./pengguna/getPengguna"));
// DISPLAYPENGGUNASORTBYDATE
router.use(
  "/getPenggunaSortByDate",
  require("./pengguna/getPenggunaSortByDate")
);
// DISPLAYPENGGUNASORTBYDATE
router.use(
  "/getPenggunaSortByDateAsc",
  require("./pengguna/getPenggunaSortByDateAsc")
);
// DISPLAYBUTIRANPENGGUNA
router.use("/getPenggunaButiran", require("./pengguna/getPenggunaButiran"));
//END PENGGUNA SECTION

/* (AKMALSAB) */
router.use("/tempahan", require("./ciptaTempahan/tempahanBaru"));

/* (LUHMAN) */
router.use(
  "/updateLiveStoktoInActive",
  require("./liveStok/updateLiveStoktoInActive")
);
router.use(
  "/updateLiveStoktoActive",
  require("./liveStok/updateLiveStoktoActive")
);
router.use("/deleteLiveStok", require("./liveStok/deleteLiveStok"));

/* (AKMALSAB) */

/* (ISMAT) */
router.use("/getAktivitiWithDate", require("./aktiviti/getAktivitiWithDate"));
router.use(
  "/getSenaraiStokTarikhList",
  require("./aktiviti/getSenaraiStokTarikhList")
);
router.use("/getSenaraiStok", require("./aktiviti/getSenaraiStok"));
router.use("/getSelectedWakil", require("./aktiviti/getSelectedWakil"));
router.use(
  "/getAktivitiDateLivestok",
  require("./aktiviti/getAktivitiDateLivestok")
);
router.use("/getNotificationCount", require("./aktiviti/getNotificationCount"));
router.use("/getNotificationLog", require("./aktiviti/getNotificationLog"));
router.use(
  "/insertAktivitiDateLivestok",
  require("./aktiviti/insertAktivitiDateLivestok")
);
router.use("/pindahStok", require("./aktiviti/pindahStok"));
router.use(
  "/updateAktivitiDateLivestok",
  require("./aktiviti/updateAktivitiDateLivestok")
);
router.use(
  "/sendNotificationAktiviti",
  require("./aktiviti/sendNotificationAktiviti")
);
router.use("/ciptaSijil", require("./sijil/getSijil"));
router.use(
  "/getSenaraiPesertaSijil",
  require("./sijil/getSenaraiPesertaSijil")
);
router.use("/uploadSijil", require("./sijil/uploadSijil"));
router.use("/getPesertaInOneAnimal", require("./sijil/getPesertaInOneAnimal"));

// (ZULAZRI)
router.use("/getSetupKorbanId", require("./dashboard/getSetupKorbanId"));

// (JULAP - IMPLEMENTER / SENARAI HAIWAN / CHANGE STOK STATUS)
router.use("/changeStokStatus", require("./haiwanKorban/changeStokStatus"));

// (JULAP - UPDATE STOK LOCATION)
router.use("/updateLokasiStok", require("./haiwanKorban/updateLokasiStok.js"));

// (JULAP - HAIWAN KORBAN)
router.use("/senaraiHaiwanKorban", require("./haiwanKorban/senaraiHaiwan"));

// (JULAP - UPDATE BUTIRAN TEMPAHAN PEMBAYAR)
router.use("/updateButiranPembayarTempahan", require("./tempahan/updateButiranPembayar"));

module.exports = router;
