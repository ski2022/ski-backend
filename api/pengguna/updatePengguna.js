const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/pengguna/pengguna.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let userId = null;
  let userFullname = null;
  let userUsername = null;
  let userEmail = null;
  let userPassword = null;
  let userPhoneNo = null;
  let userIc = null;
  let userType = null;
  let userAlamat = null;
  let userAlamatPostCode = null;
  let userAlamatBandar = null;
  let userAlamatNegeri = null;
  let userAlamatNegara = null;
  let userBankAccNo = null;
  let userBankHolderName = null;
  let userBank = null;
  let userEjenPermalink = null;
  let userEjenCategory = null;
  let userEjenCommissionCategory = null;
  let userEjenCommissionType = null;
  let userEjenCommissionValue = null;
  let userEjenIdReferal = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.body;
    // username = param.username;
    // password = param.password;
    // fullname = param.fullname;
    // type = param.type;

    userId = param.userId;
    userFullname = param.userFullname;
    userUsername = param.userUsername;
    userEmail = param.userEmail;
    userPassword = param.userPassword;
    userPhoneNo = param.userPhoneNo;
    userIc = param.userIc;
    userType = param.userType;
    userAlamat = param.userAlamat;
    userAlamatPostCode = param.userAlamatPostCode;
    userAlamatBandar = param.userAlamatBandar;
    userAlamatNegeri = param.userAlamatNegeri;
    userAlamatNegara = param.userAlamatNegara;
    userBankAccNo = param.userBankAccNo;
    userBankHolderName = param.userBankHolderName;
    userBank = param.userBank;
    userEjenPermalink = param.userEjenPermalink;
    userEjenCategory = param.userEjenCategory;
    userEjenCommissionCategory = param.userEjenCommissionCategory;
    userEjenCommissionType = param.userEjenCommissionType;
    userEjenCommissionValue = param.userEjenCommissionValue;
    userEjenIdReferal = param.userEjenIdReferal;


    //VALIDATE UNIQUE USERNAME AND EMAIL

    let checkUsername = await model.checkUsername(userId, userUsername)
    let checkEmail = await model.checkEmail(userId, userEmail)
    let checkPhone = await model.checkPhone(userId, userPhoneNo)
    console.log(checkPhone)
    let message = null

    // if (checkUsername == false) {
    //   if(checkEmail == false){
    //     if(checkPhone == false){
    //       result = {
    //         status: "gagal",
    //         message: 'Username, Email & Nombor Telefon telah wujud.'
    //     }
    //   }
      
    //   }
    // }

    if(checkUsername == false || checkEmail == false || checkPhone == false){
      console.log('Masuk banyak')
      if (checkUsername == false){
        message = 'Username telah wujud.'
        result = {
              status: "gagal",
              message: message
            }
      }
      else{
        console.log('Update Username')
      }
      if (checkEmail == false){
        console.log('Email wujud')
        message += ' | Email telah wujud.'
        result = {
              status: "gagal",
              message: message
            }

      }
      else{
        console.log('Update Email')
      }
      if (checkPhone == false){
        console.log('Phone wujud')
        message += ' | Phone telah wujud.'
        result = {
              status: "gagal",
              message: message
            }
      }
      else{
        console.log('Update Phone')
      }
    }

    // if (checkUsername == false){
    //   result = {
    //     status: "gagal",
    //     message: 'Username telah wujud.'
    //   }
    // }

    // if(checkEmail == false){
    //   result = {
    //     status: "gagal",
    //     message: 'Email telah wujud.'
    //   }
    // }
      
    // if(checkPhone == false){
    //   result = {
    //     status: "gagal",
    //     message: 'Nombor Telefon telah wujud.'
    //   }   
    // }
    else{
        if (userType == 'Wakil') {
            let updateUser = await model.updateUser(
                userId,
                userFullname,
                userUsername,
                userEmail,
                userPassword,
                userPhoneNo,
                userIc,
                userType,
                userAlamat,
                userAlamatPostCode,
                userAlamatBandar,
                userAlamatNegeri,
                userAlamatNegara,
                userBankAccNo,
                userBankHolderName,
                userBank
              )
              //INSERT USER EJEN DETAIL
              let updateUserWakil = await model.updateUserWakil(
                userId
              )
          
              if (updateUser != false || updateUserWakil != false) {
                result = {
                  status: "berjaya",
                  message: "Anda berjaya masukkan Wakil.",
                };
              } else {
                result = {
                  status: "gagal",
                  message: 'Anda gagal masukkan Wakil'
                }
              }
        }
        if (userType == 'Admin') {
            let updateUserAdmin = await model.updateUserAdmin(
                userId,
                userFullname,
                userUsername,
                userEmail,
                userPassword,
                userPhoneNo,
                userIc,
                userType,
                userAlamat,
                userAlamatPostCode,
                userAlamatBandar,
                userAlamatNegeri,
                userAlamatNegara,
                userBankAccNo,
                userBankHolderName,
                userBank
              )
              //INSERT USER EJEN DETAIL
          
              if (updateUserAdmin != false) {
                result = {
                  status: "berjaya",
                  message: "Anda berjaya masukkan Wakil.",
                };
              } else {
                result = {
                  status: "gagal",
                  message: 'Anda gagal masukkan Wakil'
                }
              }
        }
        if (userType == 'Staff') {
            let updateUserStaff = await model.updateUserStaff(
                userId,
                userFullname,
                userUsername,
                userEmail,
                userPassword,
                userPhoneNo,
                userIc,
                userType,
                userAlamat,
                userAlamatPostCode,
                userAlamatBandar,
                userAlamatNegeri,
                userAlamatNegara,
                userBankAccNo,
                userBankHolderName,
                userBank
              )
              //INSERT USER EJEN DETAIL
          
              if (updateUserStaff != false) {
                result = {
                  status: "berjaya",
                  message: "Anda berjaya masukkan Wakil.",
                };
              } else {
                result = {
                  status: "gagal",
                  message: 'Anda gagal masukkan Wakil'
                }
              }
        } 
        if (userType == 'Ejen') {
            let updateUserEjen = await model.updateUserEjen(
                userId,
                userFullname,
                userUsername,
                userEmail,
                userPassword,
                userPhoneNo,
                userIc,
                userType,
                userAlamat,
                userAlamatPostCode,
                userAlamatBandar,
                userAlamatNegeri,
                userAlamatNegara,
                userBankAccNo,
                userBankHolderName,
                userBank
              )
              //INSERT USER EJEN DETAIL
              let updateUserEjen2 = await model.updateUserEjen2(
                userId,userEjenPermalink,userEjenCategory, userEjenCommissionCategory,userEjenCommissionType, userEjenCommissionValue,userEjenIdReferal
              )
          
              if (updateUserEjen != false || updateUserEjen2 != false) {
                result = {
                  status: "berjaya",
                  message: "Anda berjaya masukkan Wakil.",
                };
              } else {
                result = {
                  status: "gagal",
                  message: 'Anda gagal masukkan Wakil'
                }
              }
        }
    } 
    


  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
