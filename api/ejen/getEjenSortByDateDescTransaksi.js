const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ejen/ejen.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let userType = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        userType = param.userType;
    
        // GET USER FUNCTION
        let getEjenSortByDateDescTransaksi = await model.getEjenSortByDateDescTransaksi(userType);
    
        if (getEjenSortByDateDescTransaksi[0] != false && getEjenSortByDateDescTransaksi[1] != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai ejen",
            environment: process.env.ENVIRONMENT,
            userdatas: getEjenSortByDateDescTransaksi[0][0],
            count: getEjenSortByDateDescTransaksi[1],
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai ejen'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;