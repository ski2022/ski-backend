const express = require("express");
const router = express.Router();
const  { getTahunKorban } = require("../../function/configuration/configuration.js");

router.get("/", async (req, res) => {
  try {
    const result = await getTahunKorban();

    if (result.tahunkorban.length > 0)
      return res
        .status(200)
        .json({ status: 200, message: "Maklumat tahun korban dijumpai!", data: result });

    res
      .status(404)
      .json({ status: 404, message: "Maklumat tahun korban tidak dijumpai!", data: "" });
  } catch (error) {
    console.error(error); // LOG ERROR

    return res
      .status(500)
      .json({
        status: 500,
        message: "Masalah Ralat! Sila Hubungi Admin.",
        data: "",
      });
  }
});

module.exports = router;