const express = require("express");
const router = express.Router();
const model = require("../../function/configuration/configuration.js");

router.post("/", async (req, res) => {
  try {
    let param = req.body;

    console.log("param", param);

    let process = await model.insertTempatKorban(param);

    return res.status(200).json(process);
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      status: 500,
      message: "Masalah Ralat! Sila Hubungi Admin.",
      data: "",
    });
  }
});

module.exports = router;
