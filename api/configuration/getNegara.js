const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/configuration/configuration.js"); // INCLUDE FUNCTION FILE

router.get("/", async (req, res) => {
  let result = null;

  try {

    // BIND PARAMETER TO VARIABLES
      const config = await model.getNegara();

      if (config != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai negara.",
          data: config
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai negara.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;