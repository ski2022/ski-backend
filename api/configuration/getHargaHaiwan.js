const express = require("express");
const router = express.Router();
const {
  getHargaHaiwan,
} = require("../../function/configuration/configuration.js");

router.get("/", async (req, res) => {
  try {
    const result = await getHargaHaiwan();

    if (result.hargahaiwan.length > 0)
      return res.status(200).json({
        status: 200,
        message: "Maklumat harga haiwan dijumpai!",
        data: result,
      });

    res.status(404).json({
      status: 404,
      message: "Maklumat harga haiwan tidak dijumpai!",
      data: "",
    });
  } catch (error) {
    console.error(error); // LOG ERROR

    return res.status(500).json({
      status: 500,
      message: "Masalah Ralat! Sila Hubungi Admin.",
      data: "",
    });
  }
});

module.exports = router;
