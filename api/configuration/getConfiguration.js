const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/configuration/configuration.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let confLookupGroupCode = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    confLookupGroupCode = param.configGroupCode;
    if (confLookupGroupCode == null || confLookupGroupCode == "") {
      result = {
        status: "gagal",
        message: 'Data tidak mencukupi.'
      }
    } else {
      // GET USER FUNCTION
      const config = await model.getConfig(confLookupGroupCode);

      if (config != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai ejen",
          data: config
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai ejen'
        }
      }


    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;