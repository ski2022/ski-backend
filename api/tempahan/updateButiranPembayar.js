const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const moment = require("moment");
const knex = require("../../connection.js");

router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  try {
    param = req.body;
    console.log(param);

    // update tempahan
    const update = await updateButiranPembayar(param);
    if (!update)
      result = {
        status: "gagal",
        message: "Gagal mengemaskini butiran pembayar.",
      };

    result = {
      status: "berjaya",
      message: "Butiran pembayar berjaya dikemaskini",
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

async function updateButiranPembayar(body) {
  // selet user info
  const user = await knex
    .connect("user")
    .leftJoin("userWakil", "userWakil.userid", "user.userId")
    .leftJoin("tempahan", "tempahan.userWakilId", "userWakil.userWakilId")
    .where("tempahan.tempahanId", body.tempahan)
    .first();

  if (!user) return false;

  // update user info
  const update = await knex
    .connect("user")
    .update({
      userFullname: body.nama,
      userEmail: body.emel,
      userPhoneNo: body.phone,
    })
    .where("userId", user.userId);

  if (!update) return false;
  return true;
}

module.exports = router;
