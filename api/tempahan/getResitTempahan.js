const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");
const CryptoJS = require("crypto-js");

router.get("/", async (req, res) => {
    let param = null;
    let result = null;
    let tempahanId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;

        tempahanId = param.tempahanId;

        // GET RESIT
        let getResit = await model.getResit(tempahanId);

        // GET TEMPAHAN DETAILS LIST
        let getTempahanDetailsResit = await model.getTempahanDetailsResit(tempahanId);

        console.log(getResit)

        console.log(getTempahanDetailsResit)

        if (getResit != null && getTempahanDetailsResit != null) {
            result = {
                status: 'Berjaya',
                message: 'Anda berjaya dapatkan butiran resit & senarai butiran tempahan details',
                data: getResit,
                data2: getTempahanDetailsResit,
            };
        } else {
            result = {
                status: 'Gagal',
                message: 'Anda gagal dapatkan butiran resit & senarai butiran tempahan details',
            };
        }

    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

router.post("/kongsiResit",  async (req, res) => {
    let param = null;
    let data = null;
    let data2 = null;
    let status = null;
    let message = null;
    let result = null;
    let userPhoneNo = null;
    let otpWakil = null;
    let smsText = null;
    let NoTempahanEncrypted = null;

    console.log(req.body)
    
    try {

      // BIND PARAMETER TO VARIABLES
      param = req.body;

      NoTempahan = param.NoTempahan;

      if(NoTempahan == null || NoTempahan == ''){
          status = "Failed"
          message = "NoTempahan cannot be empty"
      }
      else {
        // GET USER FUNCTION
        let getUserWakilDetails = await model.getUserWakilDetails(NoTempahan);        
        if (!getUserWakilDetails) return; 
        else {
          console.log('getUserWakilDetails')
          console.log(getUserWakilDetails)
          data = getUserWakilDetails[0].userPhoneNo
          userPhoneNo = getUserWakilDetails[0].userPhoneNo
        }

        var b64 = CryptoJS.AES.encrypt(NoTempahan.toString(), 'R@iden28').toString()
        var e64 = CryptoJS.enc.Base64.parse(b64)
        var eHex = e64.toString(CryptoJS.enc.Hex)
        NoTempahanEncrypted = eHex

        smsText = 'Berikut adalah pautan resit anda, ' + process.env.HOMEPAGE + 'resit-tempahan/' + NoTempahanEncrypted;

        //  SEND RECEIPT LINK VIA SMS TO USER
        let sendSMS = await model.sendSMS(smsText,userPhoneNo);        
        if (!sendSMS) return; 
        else {
          console.log('sendSMS')
          console.log(sendSMS)
          data2 = sendSMS
        }
      }           

      result = {
        status: status,
        message: message,
        environment: process.env.ENVIRONMENT,
        data: data,
        data2: data2,
      };
    } 
    catch (error) {
      console.log(error); // LOG ERROR
      result = {
        message: `API Error`,
      };
    }

    // RETURN
    res.status(200).json(result);

});


module.exports = router;
