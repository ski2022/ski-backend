
const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;
    let implementerId = null;
    let status = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;
        status = param.status;


        let getTempahanImplementerByStatus = await model.getTempahanImplementerByStatus(implementerId, status);

        if (getTempahanImplementerByStatus != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer mengikut status.",
                data: getTempahanImplementerByStatus,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer mengikut status.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;