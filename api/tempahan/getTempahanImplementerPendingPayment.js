const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let implementerId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;

        let getTempahanImplementerPendingPayment = await model.getTempahanImplementerPendingPayment(implementerId);

        if (getTempahanImplementerPendingPayment[0] != false || getTempahanImplementerPendingPayment[1] != false || getTempahanImplementerPendingPayment[2] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer.",
                data: getTempahanImplementerPendingPayment[0],
                data1: getTempahanImplementerPendingPayment[1],
                data2: getTempahanImplementerPendingPayment[2],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;