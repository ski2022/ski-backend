const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let implementerId = null;
    let tempahanStatus = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;
        tempahanStatus = param.tempahanStatus;

        let getLivestokAndPeserta = await model.getLivestokAndPeserta(implementerId,tempahanStatus);
        if(!getLivestokAndPeserta){
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer.'
            }
        }
        else{
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer.",
                data: getLivestokAndPeserta[0],
            };
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;