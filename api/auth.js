const express = require("express");
const router = express.Router();
const knex = require("../connection.js");

router.get("/", async (req, res) => {
  console.log("GET request param body", req.query);
  let userID = req.query.id ? req.query.id : "";
  const getUser = await knex.connect("user").where("userId", userID);
  if (!getUser) return;
  let results = {
    message: "anda berjaya GET",
    environment: process.env.ENVIRONMENT,
    userdata: getUser,
  };
  res.status(200).json(results);
});

router.post("/", async (req, res) => {
  console.log("POST request param body", req.body);
  let userID = req.body.id ? req.body.id : "";
  const getUser = await knex.connect("user").where("userId", userID);
  if (!getUser) return;
  let results = {
    message: "anda berjaya POST",
    environment: process.env.ENVIRONMENT,
    userdata: getUser,
  };
  res.status(200).json(results);
});

router.get("/add", (req, res) => {
  console.log("request param body", req.body);
  let results = {
    message: "anda tidak berjaya tambah",
  };
  res.status(401).json(results);
});

module.exports = router;
