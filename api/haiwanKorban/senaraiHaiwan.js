const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/senaraiHaiwan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  try {
    // GET SENARAI HAIWAN
    var result = await model.get_senarai_haiwan(req.body);

    return res.status(200).json({
      status: "berjaya",
      message: "Senarai haiwan berjaya",
      data: result,
    });
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }
});

module.exports = router;
