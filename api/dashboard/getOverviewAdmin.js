const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let param = req.query;
  let year = param.Id;

  // console.log("YEAR KE NI", year)
  try {
    let getOverviewAdmin = await model.getOverviewAdmin(year);

    if (getOverviewAdmin[0] != false && getOverviewAdmin[1] != false && getOverviewAdmin[2] != false &&getOverviewAdmin[3] != false && getOverviewAdmin[4] != false && getOverviewAdmin[5] != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan data Overview",
        environment: process.env.ENVIRONMENT,
        overviewData1: getOverviewAdmin[0][0],
        overviewData2: getOverviewAdmin[1][0],
        overviewData3: getOverviewAdmin[2][0],
        overviewData4: getOverviewAdmin[3][0],
        overviewData5: getOverviewAdmin[4][0],
        overviewData6: getOverviewAdmin[5][0]
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan data Overview'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;