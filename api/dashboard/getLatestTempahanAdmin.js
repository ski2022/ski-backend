const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let result = null;

    try {
        let param = req.query;
        let year = param.Id;
    
        // console.log("YEAR", year)

        let getLatestTempahanAdmin = await model.getLatestTempahanAdmin(year);

        if (getLatestTempahanAdmin != null) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan data Tempahan Terkini",
                environment: process.env.ENVIRONMENT,
                latestTempahanData: getLatestTempahanAdmin,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan data Tempahan Terkini'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;