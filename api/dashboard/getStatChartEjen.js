const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let param = null;
  let userId = null;


  try {
    param = req.query
    userId = param.userId
    let getStatChartEjen = await model.getStatChartEjen(userId);

    if (getStatChartEjen[0] != false && getStatChartEjen[1] != false && getStatChartEjen[2] != false && getStatChartEjen[3] != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan data Stat Chart",
        environment: process.env.ENVIRONMENT,
        statChartData1: getStatChartEjen[0][0],
        statChartData1_1: getStatChartEjen[1][0],
        statChartData2: getStatChartEjen[2][0],
        statChartData2_1: getStatChartEjen[3][0],
        statChartData3: getStatChartEjen[4][0],
        statChartData3_1: getStatChartEjen[5][0],
        statChartData4: getStatChartEjen[6][0],
        statChartData4_1: getStatChartEjen[7][0]
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan data Stat Chart'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;