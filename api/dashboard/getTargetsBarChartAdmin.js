const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let result = null;
    let param = null;
    let jenisHaiwan = null;
    let year = null;


    try {
        param = req.query;
        jenisHaiwan2 = param.Haiwan2;
        year = param.Id;

        let getTargetsBarChartAdmin = await model.getTargetsBarChartAdmin(jenisHaiwan2, year);

        if (getTargetsBarChartAdmin != null) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan data Targets Bar Chart",
                environment: process.env.ENVIRONMENT,
                targetsBarChartData: getTargetsBarChartAdmin,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan data Targets Bar Chart'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;