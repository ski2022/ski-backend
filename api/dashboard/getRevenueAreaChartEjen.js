const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let filter = null;
  let userId = null;
  let param = null;

  try {
    param = req.query;
    filter = param.filter;
    userId = param.userId;
    let getRevenueAreaChartEjen = await model.getRevenueAreaChartEjen(userId, filter);

    if (getRevenueAreaChartEjen != false ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan data Revenue Chart",
        environment: process.env.ENVIRONMENT,
        revenueAreaChartData: getRevenueAreaChartEjen[0],
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan data Revenue Chart'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;